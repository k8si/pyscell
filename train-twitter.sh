#!/usr/bin/env bash

dir="tiny"
train="$dir/train.txt"
val="$dir/val.txt"
test="$dir/test.txt"
vocab="$dir/vocab.txt"

python3 pyscell/train_lstm.py $train $val $test $vocab \
--batch-size 100 \
--bidir 1 \
--decay 0.95 \
--dropout 0.1 \
--early-stop 5 \
--learning-rate 0.002 \
--nlayers 2 \
--no-decay-epochs 10 \
--nrec 128 \
--num-epochs 50 \
--seqlen 140 \
--val-freq 1000




