#!/usr/bin/env bash

table="resources/emoji_table.txt"

ddir="twitter"
vocab="twitter/vocab.txt"

python pyscell/label_file.py $ddir/train.txt $vocab $table

python pyscell/label_file.py $ddir/val.txt $vocab $table --emo-vocab $ddir/train.txt.lab.vocab_emo

python pyscell/label_file.py $ddir/test.txt $vocab $table --emo-vocab $ddir/train.txt.lab.vocab_emo

##python pyscell/label_file.py $ddir/train.txt $ddir/vocab.txt --emotable $table
#python pyscell/label_file.py $ddir/train.txt $vocab --emotable $table
#
#replace_char=`cat $ddir/train.txt.lab.replace_char`
#
#python pyscell/label_file.py $ddir/val.txt $ddir/train.txt.lab.vocab_char \
#--emotable $table \
#--emo-vocab $ddir/train.txt.lab.vocab_emo \
#--replace-char $replace_char
#
#python pyscell/label_file.py $ddir/test.txt $ddir/train.txt.lab.vocab_char \
#--emotable $table \
#--emo-vocab $ddir/train.txt.lab.vocab_emo \
#--replace-char $replace_char