#!/usr/bin/env bash

dir="debug"

infile=$1
outfile=$2

echo "infile: $infile , outfile: $outfile"

chvocab="$dir/vocab.txt"
chvocabsize=`cat $chvocab | wc -l`
chmodel="$dir/2016-04-21-h23m59-chemb.pkl"

python pyscell/pickle_data.py $infile $outfile \
--ch-bidir 1 \
--ch-dropout 0.1 \
--ch-vocab-size $chvocabsize \
--ch-model-file $chmodel \
--ch-nrec 128 \
--ch-nlayers 2 \
--ch-seqlen 100





