#!/usr/bin/env bash

dir="debug"
train="$dir/train.txt.lab"
val="$dir/val.txt.lab"
test="$dir/test.txt.lab"
vocab="$dir/train.txt.lab.vocab_emo"
model="$dir/classify_model.pkl"

chvocab="$dir/vocab.txt"
chvocabsize=`cat $chvocab | wc -l`
chmodel="$dir/2016-04-21-h23m59-chemb.pkl"


python pyscell/train_classifier.py $train $val $test $vocab $rchar \
--model-file $model \
--ch-bidir 1 \
--ch-dropout 0.1 \
--ch-vocab-size $chvocabsize \
--ch-model-file $chmodel \
--ch-nrec 128 \
--ch-seqlen 100 \
--batch-size 100 \
--decay 0.95 \
--early-stop 5 \
--learning-rate 0.002 \
--nlayers 2 \
--no-decay-epochs 10 \
--num-epochs 50 \
--val-freq 10




