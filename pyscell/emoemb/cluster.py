import numpy as np
from sklearn.cluster import KMeans, SpectralClustering
from scipy.spatial.distance import cosine


RNG = np.random.RandomState(1234)


def cluster(M, nclusters):
    kmeans = KMeans(n_clusters=nclusters, max_iter=1000, random_state=RNG)
    # kmeans = SpectralClustering(n_clusters=nclusters)
    kmeans.fit(M)
    preds = kmeans.predict(M)
    # preds = kmeans.fit_predict(M)
    clus2eix = {cid: [] for cid in xrange(nclusters)}
    for i in xrange(M.shape[0]):
        cid = preds[i]
        clus2eix[cid].append(i)
    trans = kmeans.transform(M)
    # trans = kmeans.affinity_matrix_
    ctrs = kmeans.cluster_centers_
    print 'ctrs', ctrs.shape
    clus2exemp = {cid: None for cid in xrange(nclusters)}
    for cid, eixes in clus2eix.items():
        dists = []
        for eix in eixes:
            sample = M[eix, :]
            ctr = ctrs[cid, :]
            dist = cosine(ctr, sample)
            dists.append(dist)

        # dists = np.asarray([trans[eix, cid] for eix in eixes])
        mindist_ix = np.argmin(dists)
        exemp_ix = eixes[mindist_ix]
        clus2exemp[cid] = exemp_ix
    print kmeans.cluster_centers_
    return clus2eix, clus2exemp
