# -*- coding: utf-8 -*-

import re

patterns = [
    re.compile('(?P<emo>[（()）]+[~-]?(\s)*[~-]?[=:;；]+)', flags=re.UNICODE),
    re.compile('(?P<emo>[=:;；]+[~-]?(\s)*[~-]?[)）]+)', flags=re.UNICODE),
    re.compile('(?P<emo>\W{1,2}[（(]+\s*\W{2,10}\s*[)）]\W{1,2})', flags=re.UNICODE),
    re.compile('(?P<emo>[（(]+\s*\W{2,10}\s*[（()）]+)', flags=re.UNICODE),
    re.compile('(?P<emo>[（(]+\s*\W{2,10}\s*[)）]+\W{1,4})', flags=re.UNICODE),
    re.compile('(?P<emo>\W{1,4}[（(]+\s*\W{2,10}\s*[)）]+)', flags=re.UNICODE)
]

