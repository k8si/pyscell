from __future__ import division
import numpy as np
import theano
import theano.tensor as T
import time
import lasagne
from lasagne import layers
from lasagne.layers import get_output
from data_io import util
import sys
from collections import OrderedDict

print 'THEANO VERSION', theano.__version__
print 'LASAGNE VERISON', lasagne.__version__


REC_NUM_UNITS = 10
TOL = 1e-6                          # numerial stability
INI = lasagne.init.Uniform(0.1)     # initial parameter values
lr = 2e-3                           # learning rate
batchsize = 1
num_epochs = 5

def load_char_embeddings_and_vocab(embfile, vocabfile):
    ch2ix = util.load_char_vocab(vocabfile)
    ix2ch = {ix: ch for ch, ix in ch2ix.items()}
    ch2emb = {}
    ch2ix_prune = {}
    with open(embfile, 'r') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            ix = int(parts[0])
            ch = ix2ch[ix]
            rest = parts[1]
            vec = np.asarray(map(float, rest.split(' ')))
            ch2emb[ch] = vec
            ch2ix_prune[ch] = ix
    return ch2emb, ch2ix_prune


def load_words(wfile):
    word2ix = {}
    ix = 0
    with open(wfile, 'r') as fr:
        for line in fr.readlines():
            word = line.rstrip('\n')
            word2ix[word] = ix
            ix += 1
    return word2ix


def build_network(maxlen, embedding_size, V, invar=None, maskvar=None):
    net = OrderedDict()
    net['inp'] = layers.InputLayer((None, maxlen, embedding_size), input_var=invar)
    net['mask'] = layers.InputLayer((None, maxlen), input_var=maskvar)
    def create_gate():
        return layers.Gate(W_in=INI, W_hid=INI, W_cell=None)
    net['gru'] = layers.GRULayer(net['inp'],
                            num_units=REC_NUM_UNITS,
                            resetgate=create_gate(),
                            updategate=create_gate(),
                            hidden_update=create_gate(),
                            learn_init=True,
                            mask_input=net['mask']
                            )
    net['out'] = layers.DenseLayer(net['gru'], num_units=V, nonlinearity=lasagne.nonlinearities.softmax)
    check = {net['inp']: (1, maxlen, embedding_size), net['mask']: (1, 10)}
    print layers.get_output_shape(net['gru'], input_shapes=check)
    print layers.get_output_shape(net['out'], input_shapes=check)
    return net


wordsfile = sys.argv[1]
embfile = sys.argv[2]
vocabfile = sys.argv[3]

ch2emb, ch2ix = load_char_embeddings_and_vocab(embfile, vocabfile)
ix2ch = {ix: ch for ch, ix in ch2ix.items()}
word2ix = load_words(wordsfile)
V = len(word2ix)
maxlen = max(map(lambda (w, ix): len(w), word2ix.items()))
embedding_size = ch2emb.values()[0].shape[0]
print 'maxlen:', maxlen
print 'emb dim:', embedding_size
print 'word vocab:', V

def get_data():
    N = len(word2ix)
    W = np.zeros((N, maxlen, embedding_size))
    M = np.zeros((N, maxlen))
    Y = np.zeros((N,))
    for word, ix in word2ix.items():
        for j in xrange(len(word)):
            ch = word[j]
            emb = ch2emb[ch]
            W[ix, j, :] = emb
            M[ix, j] = 1
        Y[ix] = ix
    return W, M, Y


xvar = T.tensor3()
maskvar = T.imatrix()
yvar = T.imatrix()

net = build_network(maxlen, embedding_size, V, invar=xvar, maskvar=maskvar)

output = lasagne.layers.get_output(net['out'])
cost = lasagne.objectives.categorical_crossentropy(output, yvar).mean()
preds = T.argmax(output, axis=1)
params = lasagne.layers.get_all_params(net.values())
grad_updates = lasagne.updates.adam(cost, params)
print 'compiling f_train'
f_train = theano.function([xvar, maskvar, yvar], [cost, preds],
                        updates=grad_updates,
                        allow_input_downcast=True)

X, XM, Y = get_data()
N = X.shape[0]

for epoch in xrange(num_epochs):
    print 'starting epoch ', epoch
    ixes = np.arange(N)
    np.random.shuffle(ixes)
    losses = []
    for i in xrange(N):
        ix = ixes[i]
        x = X[ix, :, :]
        xmask = XM[ix, :]
        y = Y[ix]
        loss, ypred = f_train(x, xmask, y)
        losses.append(loss)
        if i % 10 == 0:
            correct = ypred[ypred == y]
            print 'epoch', epoch, 'batch', i, 'acc:', correct/len(ypred)

    print 'epoch', epoch, 'loss:', np.mean(losses)






