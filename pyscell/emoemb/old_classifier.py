import sys
from data_io import util
import  numpy as np
import theano
import theano.tensor as T
import lasagne
from lasagne import layers
from collections import OrderedDict
import cPickle
import codecs
from sklearn.metrics import f1_score, classification_report

print 'THEANO VERSION', theano.__version__
print 'LASAGNE VERISON', lasagne.__version__


REC_NUM_UNITS = 10
TOL = 1e-6                          # numerial stability
INI = lasagne.init.Uniform(0.1)     # initial parameter values
BATCHSIZE = 100
lr = 2e-3                           # learning rate
NUM_EPOCHS = 10
VAL_FREQ = 50

thelog = codecs.open('log.txt', 'w', 'utf8')


def dolog(msg):    
    thelog.write(msg + '\n')


def build_network(maxlen, embedding_size, invar=None, maskvar=None):
    net = OrderedDict()
    net['inp'] = layers.InputLayer((BATCHSIZE, maxlen, embedding_size), input_var=invar)
    net['mask'] = layers.InputLayer((BATCHSIZE, maxlen), input_var=maskvar)

    gate_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        b=lasagne.init.Constant(0.)
    )
    cell_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        W_cell=None,
        b=lasagne.init.Constant(0.),
        nonlinearity=lasagne.nonlinearities.tanh
    )
    net['fwd1'] = layers.LSTMLayer(
        net['inp'],
        num_units=REC_NUM_UNITS,
        nonlinearity=lasagne.nonlinearities.tanh,
        mask_input=net['mask'],
        ingate=gate_params,
        forgetgate=gate_params,
        cell=cell_params,
        outgate=gate_params,
        learn_init=True,
        only_return_final=True
    )
    net['dropout1'] = layers.DropoutLayer(net['fwd1'], p=0.5)
    net['out'] = layers.DenseLayer(net['dropout1'], num_units=2, nonlinearity=lasagne.nonlinearities.softmax)
    check = {net['inp']: (1, maxlen, embedding_size), net['mask']: (1, maxlen)}
    print layers.get_output_shape(net['fwd1'], input_shapes=check)
    print layers.get_output_shape(net['dropout1'], input_shapes=check)
    print layers.get_output_shape(net['out'], input_shapes=check)
    return net

trainfile = sys.argv[1]  # training examples
valfile = sys.argv[2]    # validation examples
embfile = sys.argv[3]    # character embeddings
vocabfile = sys.argv[4]  # character vocab

# ch2ix = util.load_char_vocab(vocabfile)
ch2ix = cPickle.load(open(vocabfile, 'r'))
ix2ch = {ix: ch for ch, ix in ch2ix.items()}

def load_chemb():
    ix2emb = {}
    with open(embfile, 'r') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            assert len(parts) == 2
            ix = int(parts[0])
            assert ix in ix2ch
            vec = np.asarray(map(float, parts[1].split(' ')))
            ix2emb[ix] = vec.astype(theano.config.floatX)
    return ix2emb

ix2emb = load_chemb()
embedding_dim = ix2emb.values()[0].shape[0]
print 'embedding_dim:', embedding_dim
dolog('embedding_dim: %d' % (embedding_dim))

def load_data():

    def padmask(x, maxlen):
        n = x.shape[0]
        xpad = np.zeros((maxlen, embedding_dim))
        xmask = np.zeros((maxlen,))
        if n < maxlen:
            xpad[:n, :] = x
            xmask[:n] = 1
        else:
            xpad[:, :] = x
            xmask[:] = 1
        return xpad, xmask

    def readfile(fname):
        X, Y = [], []
        S = []

        labelfreq = {0: 0, 1: 0}
        errfreq = {0: 0, 1: 0}

        N = 0
        errors = 0
        total = 0

        with codecs.open(fname, 'r', 'utf8') as fr:
            for line in fr.readlines():
                line = line.rstrip('\n')
                parts = line.split('\t')
                label = int(parts[0])
                if label < 0:
                    label = 0
                string = parts[1]

                err = False

                x = np.zeros((len(string), embedding_dim))
                for j, ch in enumerate(string):
                    try:
                        ix = ch2ix[ch]
                    except KeyError:
                        try:
                            uch = unicode(ch)
                            ix = ch2ix[uch]
                        except Exception:
                            err = True
                            continue
                    emb = ix2emb[ix]
                    x[j, :] = emb

                if not err:
                    S.append(string)
                    X.append(x)
                    Y.append(label)
                    N += 1
                else:
                    errors += 1
                    errfreq[label] += 1
                total += 1
                labelfreq[label] += 1

        print 'errors:', errors, 'out of', total
        print 'labelfreqs', labelfreq
        print 'errfreqs', errfreq
        
        logstr = 'labelfreqs: 0 %d, 1 %d' % (labelfreq[0], labelfreq[1])
        dolog(logstr)
        logstr = 'err freqs: 0 %d, 1 %d' % (errfreq[0], errfreq[1])
        dolog(logstr)

        maxlen = -1
        for x in X:
            if x.shape[0] > maxlen:
                maxlen = x.shape[0]
        Xpad = np.zeros((N, maxlen, embedding_dim), dtype='float32')
        Xmask = np.zeros((N, maxlen), dtype='int32')
        for j, x in enumerate(X):
            xp, xm = padmask(x, maxlen)
            Xpad[j, :, :] = xp
            Xmask[j, :] = xm
        return Xpad, Xmask, np.asarray(Y).astype('int32'), S, maxlen

    xtrain, xmtrain, ytrain, train_strings, maxlen1 = readfile(trainfile)
    xval, xmval, yval, val_strings, maxlen2 = readfile(valfile)
    maxlen = max(maxlen1, maxlen2)
    train = (xtrain, xmtrain, ytrain, train_strings)
    val = (xval, xmval, yval, val_strings)
    return train, val, maxlen

dtrain, dval, maxlen = load_data()

xvar = T.tensor3()
maskvar = T.imatrix()
yvar = T.ivector()

net = build_network(maxlen, embedding_dim, invar=xvar, maskvar=maskvar)

train_output = layers.get_output(net['out'], deterministic=False)
train_cost = lasagne.objectives.categorical_crossentropy(train_output, yvar).mean()
params = lasagne.layers.get_all_params(net.values())
grad_updates = lasagne.updates.adam(train_cost, params, learning_rate=lr)

val_output = layers.get_output(net['out'], deterministic=True)
val_cost = lasagne.objectives.categorical_crossentropy(val_output, yvar).mean()
val_predictions = T.argmax(val_output, axis=1)
val_accuracy = T.mean(T.eq(val_predictions, yvar))

# def compute_f1(net_output, targets):
#     preds = np.argmax(net_output, axis=1)
#     return f1_score(targets, preds)

print 'compiling f_train'
f_train = theano.function([xvar, maskvar, yvar],
                          [train_cost],
                          updates=grad_updates,
                          allow_input_downcast=True)
print 'compiling f_eval'
f_eval = theano.function([xvar, maskvar, yvar],
                         [val_cost, val_accuracy, val_predictions],
                         allow_input_downcast=True)
#test_losses = [f1_score(test_model_f1(i)[0],test_model_f1(i)[1])  for i in xrange(n_test_batches)]

xtrain, xmtrain, ytrain, train_strings = dtrain
xval, xmval, yval, val_strings = dval

print xtrain.shape, xmtrain.shape, ytrain.shape, len(train_strings)
print xval.shape, xmval.shape, yval.shape, len(val_strings)

N = xtrain.shape[0]
nbatches = N//BATCHSIZE
ixes = np.arange(N)

Nval = xval.shape[0]
nbatches_val = Nval//BATCHSIZE
val_ixes = np.arange(Nval)

thelog.flush()

for epoch in xrange(NUM_EPOCHS):
    print 'starting epoch', epoch
    losses = []
    np.random.shuffle(ixes)
    curr = 0
    for batch in xrange(nbatches):
        bix = ixes[curr:curr+BATCHSIZE]
        curr += BATCHSIZE
        xmini = xtrain[bix, :, :].astype(theano.config.floatX)
        xmini_mask = xmtrain[bix, :]
        ymini = ytrain[bix]
        loss = f_train(xmini, xmini_mask, ymini)
        losses.append(loss)

        if (batch % VAL_FREQ) == 0:
            np.random.shuffle(val_ixes)
            vlosses = []
            vaccs = []
            vpreds = np.zeros((Nval,))
            vtrues = np.zeros((Nval,))
            vcurr = 0
            doprint = True
            for vbatch in xrange(nbatches_val):
                bix = val_ixes[vcurr:vcurr+BATCHSIZE]

                xval_mini = xval[bix, :, :].astype(theano.config.floatX)
                xval_mini_mask = xmval[bix, :]
                yval_mini = yval[bix]

                vloss, vacc, vpred = f_eval(xval_mini, xval_mini_mask, yval_mini)

                for ix, v in enumerate(bix):
                    vpreds[v] = vpred[ix]
                    vtrues[v] = yval_mini[ix]

                vcurr += BATCHSIZE

                vlosses.append(vloss)
                vaccs.append(vacc)
                if doprint:
                    for j in xrange(BATCHSIZE):
                        line = '%d %d %s' % (yval[j], vpreds[j], val_strings[j])
                        dolog(line)
                    doprint = False

            vloss = np.mean(vlosses)
            vacc = np.mean(vaccs)
            vf1 = f1_score(vtrues, vpreds, average='macro')
            vf1_micro = f1_score(vtrues, vpreds, average='micro')
            print 'validation'
            print 'epoch     : %d' % epoch
            print 'batch     : %d' % batch
            print 'crossent  : %.6f' % vloss
            print 'accuracy  : %.6f' % vacc
            print 'f1        : %.6f' % vf1
            print 'f1 (micro): %.6f' % vf1_micro

            print classification_report(vtrues, vpreds, labels=[0, 1], target_names=['no', 'yes'])

            dolog('validation')
            dolog('epoch     : %d' % epoch)
            dolog('batch     : %d' % batch)
            dolog('crossent  : %.6f' % vloss)
            dolog('accuracy  : %.6f' % vacc)
            dolog('f1        : %.6f' % vf1)
            dolog('f1 (micro): %.6f' % vf1_micro)
            thelog.flush()


    mean_loss = np.mean(losses)
    # vloss, vacc, vpreds = f_eval(xval, xmval, yval)
    print 'training: done with epoch %d/%d' % (epoch, NUM_EPOCHS)    
    print 'epoch          : %d' % epoch
    print 'train crossent : %.6f' % mean_loss    
    # print 'valid crossent : %.6f' % vloss
    # print 'valid accuracy : %.6f' % vacc

    dolog('training: done with epoch %d/%d' % (epoch, NUM_EPOCHS))
    dolog('epoch          : %d' % epoch)
    dolog('train crossent : %.6f' % mean_loss)
    # dolog('valid crossent : %.6f' % vloss)
    # dolog('valid accuracy : %.6f' % vacc)
    thelog.flush()

    with open('model.pkl', 'wb') as f:
        cPickle.dump(lasagne.layers.get_all_param_values(net.values()), f, cPickle.HIGHEST_PROTOCOL)