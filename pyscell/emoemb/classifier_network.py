import lasagne
from lasagne import layers
from theano.tensor import add
from collections import OrderedDict


INI = lasagne.init.Uniform(0.1)      # initial parameter values


def tmean(a, b):
    return add(a, b) / 2.


def build_network(embedding_size,
                  nclasses,
                  dropout_frac=0.0,
                  lstm_units=1,
                  nlayers=1,
                  seqlen=None,
                  batchsize=100,
                  grad_clip=5.,
                  network_type='simple',
                  prev_var=None,
                  prev_mask=None,
                  next_var=None,
                  next_mask=None):

    net = OrderedDict()
    net['input-prev'] = layers.InputLayer((batchsize, seqlen, embedding_size), input_var=prev_var)
    net['mask-prev'] = layers.InputLayer((batchsize, seqlen), input_var=prev_mask)
    net['input-next'] = layers.InputLayer((batchsize, seqlen, embedding_size), input_var=next_var)
    net['mask-next'] = layers.InputLayer((batchsize, seqlen), input_var=next_mask)

    gate_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        b=lasagne.init.Constant(0.)
    )

    cell_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        W_cell=None,
        b=lasagne.init.Constant(0.),
        nonlinearity=lasagne.nonlinearities.tanh
    )

    if network_type == 'simple':
        build_simple(net, lstm_units, nclasses, cell_params, gate_params, grad_clip, dropout_frac)
    else:
        fwd_bwd_layers(net, lstm_units, nclasses, nlayers, cell_params, gate_params, grad_clip, dropout_frac)

    return net


def build_simple(net, nunits, nclasses, cell_params, gate_params, grad_clip, dropout_frac):
    net['fwd'] = layers.LSTMLayer(
        net['input-prev'],
        num_units=nunits,
        nonlinearity=lasagne.nonlinearities.tanh,
        ingate=gate_params,
        forgetgate=gate_params,
        cell=cell_params,
        outgate=gate_params,
        grad_clipping=grad_clip,
        learn_init=True,
        mask_input=net['mask-prev']
    )
    net['dropout_fwd'] = layers.DropoutLayer(net['fwd'], p=dropout_frac)
    net['bwd'] = layers.LSTMLayer(
        net['input-next'],
        num_units=nunits,
        nonlinearity=lasagne.nonlinearities.tanh,
        ingate=gate_params,
        forgetgate=gate_params,
        cell=cell_params,
        outgate=gate_params,
        grad_clipping=grad_clip,
        learn_init=True,
        backwards=True,
        mask_input=net['mask-next']
    )
    net['dropout_bwd'] = layers.DropoutLayer(net['bwd'], p=dropout_frac)
    net['merge'] = layers.ElemwiseSumLayer([net['dropout_fwd'], net['dropout_bwd']])
    net['output'] = layers.DenseLayer(net['merge'], num_units=nclasses, nonlinearity=lasagne.nonlinearities.softmax)


def fwd_bwd_layers(net, nunits, nclasses, nlayers, cell_params, gate_params, grad_clip, dropout_frac):
    prev_curr_layer = net['input-prev']
    next_curr_layer = net['input-next']

    for i in xrange(nlayers):
        id_prev_fwd = 'prev_fwd_%d' % i
        id_prev_fwd_drp = 'prev_fwd_drp_%d' % i
        id_prev_bwd = 'prev_bwd_%d' % i
        id_prev_bwd_drp = 'prev_bwd_drp_%d' % i
        id_prev_mrg = 'prev_mrg_%d' % i
        
        id_next_fwd = 'next_fwd_%d' % i
        id_next_fwd_drp = 'next_fwd_drp_%d' % i
        id_next_bwd = 'next_bwd_%d' % i
        id_next_bwd_drp = 'next_bwd_drp_%d' % i
        id_next_mrg = 'next_mrg_%d' % i
                
        net[id_prev_fwd] = layers.LSTMLayer(
            prev_curr_layer,
            num_units=nunits,
            nonlinearity=lasagne.nonlinearities.tanh,
            ingate=gate_params,
            forgetgate=gate_params,
            cell=cell_params,
            outgate=gate_params,
            grad_clipping=grad_clip,
            learn_init=True,
            mask_input=net['mask-prev']
        )        
        net[id_prev_fwd_drp] = layers.DropoutLayer(net[id_prev_fwd], p=dropout_frac)        
        net[id_prev_bwd] = layers.LSTMLayer(
            prev_curr_layer,
            num_units=nunits,
            nonlinearity=lasagne.nonlinearities.tanh,
            ingate=gate_params,
            forgetgate=gate_params,
            cell=cell_params,
            outgate=gate_params,
            grad_clipping=grad_clip,
            learn_init=True,
            backwards=True,
            mask_input=net['mask-prev']
        )        
        net[id_prev_bwd_drp] = layers.DropoutLayer(net[id_prev_bwd], p=dropout_frac)
        net[id_prev_mrg] = layers.ElemwiseSumLayer([net[id_prev_fwd_drp], net[id_prev_bwd_drp]])
        prev_curr_layer = net[id_prev_mrg]

        net[id_next_fwd] = layers.LSTMLayer(
            next_curr_layer,
            num_units=nunits,
            nonlinearity=lasagne.nonlinearities.tanh,
            ingate=gate_params,
            forgetgate=gate_params,
            cell=cell_params,
            outgate=gate_params,
            grad_clipping=grad_clip,
            learn_init=True,
            mask_input=net['mask-next']
        )        
        net[id_next_fwd_drp] = layers.DropoutLayer(net[id_next_fwd], p=dropout_frac)        
        net[id_next_bwd] = layers.LSTMLayer(
            next_curr_layer,
            num_units=nunits,
            nonlinearity=lasagne.nonlinearities.tanh,
            ingate=gate_params,
            forgetgate=gate_params,
            cell=cell_params,
            outgate=gate_params,
            grad_clipping=grad_clip,
            learn_init=True,
            backwards=True,
            mask_input=net['mask-next']
        )        
        net[id_next_bwd_drp] = layers.DropoutLayer(net[id_next_bwd], p=dropout_frac)
        net[id_next_mrg] = layers.ElemwiseSumLayer([net[id_next_fwd_drp], net[id_next_bwd_drp]])
        next_curr_layer = net[id_next_mrg]

    id_prev_drp = 'prev_drp_final'
    id_next_drp = 'next_drp_final'
    id_mrg = 'mrg_final'
    id_output = 'output'

    net[id_prev_drp] = layers.DropoutLayer(prev_curr_layer, p=dropout_frac)
    net[id_next_drp] = layers.DropoutLayer(next_curr_layer, p=dropout_frac)
    net[id_mrg] = layers.ElemwiseSumLayer([net[id_prev_drp], net[id_next_drp]])
    net[id_output] = layers.DenseLayer(net[id_mrg], num_units=nclasses, nonlinearity=lasagne.nonlinearities.softmax)






