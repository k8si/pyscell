from __future__ import division
from charemb import lstm_network
import load_utils
import numpy as np
import theano
from theano import pp
import theano.tensor as T
import time
import lasagne
from lasagne.layers import get_output, get_all_params
import cPickle
import codecs


RNG = np.random.RandomState(1234)


def pickle_data(args):
    print 'THEANO VERSION : %s' % theano.__version__
    print 'LASAGNE VERSION: %s' % lasagne.__version__

    infile = args.infile
    outfile = args.outfile

    CH_VOCAB_SIZE = args.ch_vocab_size
    CH_MODEL_FILE = args.ch_model_file
    CH_BIDIR = bool(args.ch_bidir)
    CH_DROPOUT_FRAC = args.ch_dropout
    CH_LSTM_LAYERS = args.ch_nlayers
    CH_LSTM_UNITS = args.ch_nrec
    CH_MODEL_SEQ_LEN = args.ch_seqlen

    """ load char embeddings """

    prev_inputs, prev_mask_inputs, next_inputs, next_mask_inputs, ydata = load_utils.load_labeled_file(infile,
                                                                                                       CH_VOCAB_SIZE,
                                                                                                       CH_MODEL_SEQ_LEN)
    print 'loaded file %s' % infile

    N = prev_inputs.shape[0]
    batch_size = 100
    n_batches = N // batch_size

    ch_invar = T.tensor3('input')
    ch_maskvar = T.imatrix('mask')
    ch_net = lstm_network.build_network(CH_VOCAB_SIZE,
                                        bidir=CH_BIDIR,
                                        dropout_frac=CH_DROPOUT_FRAC,
                                        lstm_units=CH_LSTM_UNITS,
                                        nlayers=CH_LSTM_LAYERS,
                                        batch_size=batch_size,
                                        seqlen=CH_MODEL_SEQ_LEN,
                                        invar=ch_invar,
                                        maskvar=ch_maskvar)

    ch_params = cPickle.load(open(CH_MODEL_FILE, 'r'))
    lasagne.layers.set_all_param_values(ch_net.values(), ch_params)
    print 'loaded chemb params from file %s' % CH_MODEL_FILE
    ch_layer = 'mrg_%d' % (CH_LSTM_LAYERS - 1)
    ch_out = get_output(ch_net[ch_layer], deterministic=True)

    """ compile theano function """
    print 'compiling f_lookup'
    f_lookup = theano.function([ch_invar, ch_maskvar], ch_out, allow_input_downcast=True)

    print 'f_lookup', time.ctime()
    start = time.time()

    xdata = np.zeros((N, CH_MODEL_SEQ_LEN, CH_LSTM_UNITS, 2))
    mdata = np.zeros((N, CH_MODEL_SEQ_LEN, 2))
    batch_ct = 0
    bix = 0
    while bix <= (N - batch_size):
        bstart = time.time()
        p_batch = prev_inputs[bix:bix+batch_size, :, :]
        pm_batch = prev_mask_inputs[bix:bix+batch_size, :]
        n_batch = next_inputs[bix:bix+batch_size, :, :]
        nm_batch = next_mask_inputs[bix:bix+batch_size, :]
        p_embs = f_lookup(p_batch, pm_batch)  # batchsize x seqlen x ndims
        n_embs = f_lookup(n_batch, nm_batch)
        xdata[bix:bix+batch_size, :, :, 0] = p_embs
        xdata[bix:bix+batch_size, :, :, 1] = n_embs
        mdata[bix:bix+batch_size, :, 0] = pm_batch
        mdata[bix:bix+batch_size, :, 1] = nm_batch
        bix += batch_size
        print 'done with batch %d/%d' % (batch_ct, n_batches)
        print 'took ', time.time() - bstart, 'secs'
        batch_ct += 1
        print 'total time so far', time.time() - start

    print 'saving npz...'
    np.savez_compressed(outfile, y=ydata, x=xdata, m=mdata)
    print 'saved npz'
    print 'total time', time.time() - start


