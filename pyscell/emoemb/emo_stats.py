import load_utils

emov_file = '/Users/kate/pyscell/full-rnd/train.txt.lab.vocab_emo'
emo2eix = load_utils.emos_from_file(emov_file)
eix2emo = {eix: emo for emo, eix in emo2eix.items()}
print len(emo2eix)

infile = '/Users/kate/pyscell/full-rnd/train.txt.lab.rnd'

freqs = {}

with open(infile, 'r') as fr:
    for line in fr.readlines():
        line = line.rstrip('\n')
        parts = line.split('\t')
        eid = int(parts[0])
        if eid not in freqs:
            freqs[eid] = 0
        freqs[eid] += 1

srt = sorted(freqs.items(), key=lambda item: item[1], reverse=True)
for eid, freq in srt[:20]:
    print eix2emo[eid], freq



