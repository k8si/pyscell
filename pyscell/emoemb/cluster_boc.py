import load_utils
from cluster import cluster
import numpy as np
import sys
import codecs
from scipy.spatial.distance import cosine


def avg(emo, ch2ix, ix2emb):
    n = len(emo)
    # d = ix2emb.values()[0].shape[0]
    d = len(ch2ix)
    e = np.zeros((n, d))
    for ix, ch in enumerate(emo):
        try:
            char_ix = ch2ix[ch]
        except KeyError:  # TODO should not be happening
            cp = ord(ch)
            if cp == 8209:
                char_ix = ch2ix['-']
            else:
                return None
        # emb = ix2emb[char_ix]
        e[ix, char_ix] = 1
    return np.sum(e, axis=0)


def averaged_embeddings(emofile, embfile, vocabfile):
    ch2ix = load_utils.char_vocab_from_file(vocabfile)
    ix2emb = load_utils.chemb_from_file(embfile, ch2ix)
    emo2eix = load_utils.emos_from_file(emofile)
    emo2eix_new = {}
    eix2emb = {}
    ct = 0
    errs = 0
    for emo, eix in emo2eix.items():
        emb = avg(emo, ch2ix, ix2emb)
        if emb is None:
            errs += 1
            continue
        eix2emb[ct] = emb
        emo2eix_new[emo] = ct
        ct += 1
    print errs, len(emo2eix)
    return emo2eix_new, eix2emb


def average_and_cluster(emofile, embfile, vocabfile, nclusters):
    emo2eix, eix2emb = averaged_embeddings(emofile, embfile, vocabfile)
    n = len(eix2emb)
    d = eix2emb.values()[0].shape[0]
    M = np.zeros((n, d))
    for eix, emb in eix2emb.items():
        M[eix, :] = emb
    clus2eix, clus2exemp = cluster(M, nclusters)
    eix2emo = {eix: emo for emo, eix in emo2eix.items()}
    with codecs.open('clusters_boc.txt', 'w', 'utf8') as fw:
        for cid, eixes in clus2eix.items():
            emos = [eix2emo[eix] for eix in eixes]
            exemp_ix = clus2exemp[cid]
            exemplar = eix2emo[exemp_ix]
            assert exemplar in emos
            fw.write('===> CLUSTER %d\t%s\n' % (cid, exemplar))
            lines = '\n'.join(emos)
            fw.write(lines)
            fw.write('\n\n')


def top10(emofile, embfile, vocabfile, target):
    emo2eix, eix2emb = averaged_embeddings(emofile, embfile, vocabfile)
    eix2emo = {eix: emo for emo, eix in emo2eix.items()}
    tix = emo2eix[target]
    temb = eix2emb[tix]
    dists = {}
    for emo, eix in emo2eix.items():
        if eix == tix:
            continue
        emb = eix2emb[eix]
        dist = cosine(temb, emb)
        dists[eix] = dist
    srt = sorted(dists.items(), key=lambda item: item[1])
    for eix, dist in srt[:10]:
        emo = eix2emo[eix]
        print target, emo, dist


if __name__ == '__main__':
    emof = sys.argv[1]
    embf = sys.argv[2]
    vf = sys.argv[3]
    # targ = sys.argv[4]
    # top10(emof, embf, vf, targ)
    nclus = int(sys.argv[4])
    average_and_cluster(emof, embf, vf, nclus)




