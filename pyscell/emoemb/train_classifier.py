from __future__ import division
from data_io import util, txt2oh
from charemb import lstm_network
import load_utils
from classifier_network import build_network
import numpy as np
import theano
import theano.tensor as T
import time
import lasagne
from lasagne.layers import get_output, get_all_params
import cPickle
import codecs


RNG = np.random.RandomState(1234)


def train_model(args):
    """
    Train an LSTM character-level language model
    :param args: args from an ArgumentParser
    :return: void
    """
    """ read the args """
    trainfile = args.train
    valfile = args.val
    testfile = args.test
    vocabfile = args.vocab
    # rchar = args.rchar

    modelfile = args.model_file
    load_model = bool(args.load_model)

    # CH_VOCAB_FILE = args.ch_vocab
    CH_VOCAB_SIZE = args.ch_vocab_size
    CH_MODEL_FILE = args.ch_model_file
    CH_BIDIR = bool(args.ch_bidir)
    CH_DROPOUT_FRAC = args.ch_dropout
    CH_LSTM_LAYERS = args.ch_nlayers
    CH_LSTM_UNITS = args.ch_nrec
    CH_MODEL_SEQ_LEN = args.ch_seqlen

    # BIDIR = bool(args.bidir)
    DROPOUT_FRAC = args.dropout
    LSTM_LAYERS = args.nlayers
    LSTM_UNITS = args.nrec
    MODEL_SEQ_LEN = args.seqlen
    NETWORK_TYPE = args.network_type

    BATCH_SIZE = args.batch_size
    DECAY = args.decay
    LEARNING_RATE = args.learning_rate
    EARLY_STOPPING = args.early_stop
    NO_DECAY_EPOCHS = args.no_decay_epochs
    NUM_EPOCHS = args.num_epochs
    VAL_FREQ = args.val_freq

    logger = codecs.open('log.txt', 'w', 'utf8')

    def logit(msg):
        logger.write(msg + '\n')

    logit('THEANO VERSION : %s' % theano.__version__)
    logit('LASAGNE VERSION: %s' % lasagne.__version__)
    logit(str(args))

    """ build classifier network """
    emo2eix = load_utils.emos_from_file(vocabfile)
    # eix2emo = {eix: emo for emo, eix in emo2eix.items()}
    nclasses = len(emo2eix)
    xprev_var = T.tensor3()
    mprev_var = T.imatrix()
    xnext_var = T.tensor3()
    mnext_var = T.imatrix()
    yvar = T.imatrix()

    net = build_network(CH_LSTM_UNITS,
                        nclasses,
                        dropout_frac=DROPOUT_FRAC,
                        lstm_units=LSTM_UNITS,
                        nlayers=LSTM_LAYERS,
                        seqlen=CH_MODEL_SEQ_LEN,
                        network_type=NETWORK_TYPE,
                        prev_var=xprev_var,
                        prev_mask=mprev_var,
                        next_var=xnext_var,
                        next_mask=mnext_var)

    if load_model:
        ps = cPickle.load(open(modelfile, 'r'))
        lasagne.layers.set_all_param_values(net.values(), ps)
        print 'loaded classifier model from %s' % modelfile

    def compute_accuracy(net_output, targets):
        preds = T.argmax(net_output, axis=1)
        truth = T.argmax(targets, axis=1)
        return T.mean(T.eq(preds, truth), dtype=theano.config.floatX)

    var_train_output = get_output(net['output'], deterministic=False)
    var_train_cost = lasagne.objectives.categorical_crossentropy(var_train_output, yvar).mean()
    var_train_acc = compute_accuracy(var_train_output, yvar)

    all_params = get_all_params(net.values())
    shared_rate = theano.shared(lasagne.utils.floatX(LEARNING_RATE))
    grad_updates = lasagne.updates.adam(var_train_cost, all_params, learning_rate=shared_rate)

    var_test_output = get_output(net['output'], deterministic=True)
    var_test_cost = lasagne.objectives.categorical_crossentropy(var_test_output, yvar).mean()
    var_test_predictions = T.argmax(var_test_output, axis=1)
    var_test_acc = compute_accuracy(var_test_output, yvar)

    fxn_input = [xprev_var, mprev_var, xnext_var, mnext_var, yvar]

    """ compile theano function """
    print 'compiling f_train'
    f_train = theano.function(fxn_input,
                              [var_train_cost, var_train_acc],
                              updates=grad_updates,
                              allow_input_downcast=True)
    print 'compiling f_eval'
    f_eval = theano.function(fxn_input, [var_test_cost, var_test_acc, var_test_predictions], allow_input_downcast=True)

    """ load data """
    print 'loading train data'
    prev_inputs, prev_mask_inputs, next_inputs, next_mask_inputs, ydata = load_utils.load_labeled_file(trainfile,
                                                                                                       CH_VOCAB_SIZE,
                                                                                                       CH_MODEL_SEQ_LEN)

    def reshape_ys(ys):
        ynew = np.zeros((ys.shape[0], nclasses))
        for i in xrange(ys.shape[0]):
            ynew[i, ys[i]] = 1
        return ynew

    ydata = reshape_ys(ydata)

    """ load char embeddings """
    ntrain = prev_inputs.shape[0]
    n_train_batches = ntrain // BATCH_SIZE
    train_ixes = np.arange(ntrain)

    ch_invar = T.tensor3('input')
    ch_maskvar = T.imatrix('mask')
    ch_net = lstm_network.build_network(CH_VOCAB_SIZE,
                                        bidir=CH_BIDIR,
                                        dropout_frac=CH_DROPOUT_FRAC,
                                        lstm_units=CH_LSTM_UNITS,
                                        nlayers=CH_LSTM_LAYERS,
                                        batch_size=BATCH_SIZE,
                                        seqlen=CH_MODEL_SEQ_LEN,
                                        invar=ch_invar,
                                        maskvar=ch_maskvar)

    ch_params = cPickle.load(open(CH_MODEL_FILE, 'r'))
    lasagne.layers.set_all_param_values(ch_net.values(), ch_params)
    print 'loaded chemb params from file %s' % CH_MODEL_FILE
    ch_layer = 'mrg_%d' % (CH_LSTM_LAYERS - 1)
    ch_out = get_output(ch_net[ch_layer], deterministic=True)

    """ compile theano function """
    print 'compiling f_lookup'
    f_lookup = theano.function([ch_invar, ch_maskvar], ch_out, allow_input_downcast=True)

    def get_train_batch(bix):
        batch_ixes = train_ixes[bix:bix+BATCH_SIZE]
        p_batch = prev_inputs[batch_ixes, :, :]
        pm_batch = prev_mask_inputs[batch_ixes, :]
        n_batch = next_inputs[batch_ixes, :, :]
        nm_batch = next_mask_inputs[batch_ixes, :]
        p_embs = f_lookup(p_batch, pm_batch)  # batchsize x seqlen x ndims
        n_embs = f_lookup(n_batch, nm_batch)
        Y = ydata[batch_ixes]
        return Y, p_embs, pm_batch, n_embs, nm_batch

    def evaluate(testing=False):
        if testing:
            filename = testfile
        else:
            filename = valfile
        val_prev_inputs, val_prev_mask_inputs, val_next_inputs, val_next_mask_inputs, val_ydata = \
            load_utils.load_labeled_file(filename,
                                         CH_VOCAB_SIZE,
                                         CH_MODEL_SEQ_LEN)
        val_nbatches = val_prev_inputs.shape[0] // BATCH_SIZE
        batch_ct = 0
        val_ydata = reshape_ys(val_ydata)
        losses = []
        accs = []
        nval = val_prev_inputs.shape[0]
        bix = 0
        while bix <= (nval - BATCH_SIZE):
            p_batch = prev_inputs[bix:bix+BATCH_SIZE, :, :]
            pm_batch = prev_mask_inputs[bix:bix+BATCH_SIZE, :]
            n_batch = next_inputs[bix:bix+BATCH_SIZE, :, :]
            nm_batch = next_mask_inputs[bix:bix+BATCH_SIZE, :]
            p_embs = f_lookup(p_batch, pm_batch)  # batchsize x seqlen x ndims
            n_embs = f_lookup(n_batch, nm_batch)
            l, a, preds = f_eval(p_embs, pm_batch, n_embs, nm_batch, val_ydata[bix:bix+BATCH_SIZE])
            losses.append(l)
            accs.append(a)
            print 'done with eval batch %d/%d' % (batch_ct, val_nbatches)
            bix += BATCH_SIZE
            batch_ct += 1
        vloss = np.mean(losses)
        vacc = np.mean(accs)
        return vloss, vacc

    logger.flush()

    best_test_loss = 1000.
    no_decrease_epochs = 0

    for epoch in xrange(NUM_EPOCHS):
        print 'starting epoch %d' % epoch
        print time.ctime()
        start_time = time.time()
        RNG.shuffle(train_ixes)
        train_losses = []
        train_accs = []
        curr = 0
        for batch in xrange(n_train_batches):
            ybatch, pbatch, pmbatch, nbatch, nmbatch = get_train_batch(curr)
            curr += BATCH_SIZE
            loss, acc = f_train(pbatch, pmbatch, nbatch, nmbatch, ybatch)
            train_losses.append(loss)
            train_accs.append(acc)
            if batch > 0 and (batch % VAL_FREQ) == 0:
                val_start = time.time()
                val_loss, val_acc = evaluate(testing=False)
                val_time = time.time() - val_start
                logit('--- validation')
                logit('epoch            : %d' % epoch)
                logit('batch            : %d' % batch)
                logit('took             : %d secs' % val_time)
                logit('loss (val)       : %.6f' % val_loss)
                logit('accuracy (val)   : %.6f %%' % (val_acc * 100.))
                logit('---')
                logger.flush()
                tmp_model = '%s.ep%dba%d_%.4f.chk' % (modelfile, epoch, batch, val_loss)
                with open(tmp_model, 'wb') as f:
                    cPickle.dump(lasagne.layers.get_all_param_values(net.values()), f, cPickle.HIGHEST_PROTOCOL)
                logit('saved model to %s' % tmp_model)
                logger.flush()

        epoch_time = time.time() - start_time
        train_loss = np.mean(train_losses)
        train_acc = np.mean(train_accs)
        val_loss, val_acc = evaluate(testing=False)
        test_loss, test_acc = evaluate(testing=True)

        if test_loss < best_test_loss:
            best_test_loss = test_loss
            no_decrease_epochs = 0
            logit('saving model to %s' % modelfile)
            with open(modelfile, 'wb') as f:
                cPickle.dump(lasagne.layers.get_all_param_values(net.values()), f, cPickle.HIGHEST_PROTOCOL)
        else:
            no_decrease_epochs += 1

        logit('--- done with epoch')
        logit('epoch            : %d' % epoch)
        logit('# train batches  : %d' % n_train_batches)
        logit('took             : %d secs' % epoch_time)
        logit('loss (train)     : %.6f' % train_loss)
        logit('accuracy (train) : %.6f %%' % (train_acc * 100.))
        logit('loss (val)       : %.6f' % val_loss)
        logit('accuracy (val)   : %.6f %%' % (val_acc * 100.))
        logit('loss (test)      : %.6f' % test_loss)
        logit('accuracy (test)  : %.6f %%' % (test_acc * 100.))
        logit('loss (best)      :  %.6f' % best_test_loss)
        logit('no improvements  : %d epochs' % no_decrease_epochs)
        logit('---')

        if no_decrease_epochs > EARLY_STOPPING:
            logit('stopping after %d epochs' % epoch)
            break

        if epoch > NO_DECAY_EPOCHS:
            curr_rate = shared_rate.get_value()
            new_rate = curr_rate * DECAY
            shared_rate.set_value(lasagne.utils.floatX(new_rate))

        logger.flush()

    logger.close()


