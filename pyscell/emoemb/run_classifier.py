import sys
from data_io import util
import  numpy as np
import theano
import theano.tensor as T
import lasagne
from lasagne import layers
from collections import OrderedDict
import cPickle
import codecs
from sklearn.metrics import f1_score, classification_report

print 'THEANO VERSION', theano.__version__
print 'LASAGNE VERISON', lasagne.__version__


REC_NUM_UNITS = 10
TOL = 1e-6                          # numerial stability
INI = lasagne.init.Uniform(0.1)     # initial parameter values
BATCHSIZE = 100
lr = 2e-3                           # learning rate
NUM_EPOCHS = 10
VAL_FREQ = 50


def build_network(maxlen, embedding_size, invar=None, maskvar=None):
    net = OrderedDict()
    net['inp'] = layers.InputLayer((BATCHSIZE, maxlen, embedding_size), input_var=invar)
    net['mask'] = layers.InputLayer((BATCHSIZE, maxlen), input_var=maskvar)

    gate_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        b=lasagne.init.Constant(0.)
    )
    cell_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        W_cell=None,
        b=lasagne.init.Constant(0.),
        nonlinearity=lasagne.nonlinearities.tanh
    )
    net['fwd1'] = layers.LSTMLayer(
        net['inp'],
        num_units=REC_NUM_UNITS,
        nonlinearity=lasagne.nonlinearities.tanh,
        mask_input=net['mask'],
        ingate=gate_params,
        forgetgate=gate_params,
        cell=cell_params,
        outgate=gate_params,
        learn_init=True,
        only_return_final=True
    )
    net['dropout1'] = layers.DropoutLayer(net['fwd1'], p=0.5)
    net['out'] = layers.DenseLayer(net['dropout1'], num_units=2, nonlinearity=lasagne.nonlinearities.softmax)
    check = {net['inp']: (1, maxlen, embedding_size), net['mask']: (1, maxlen)}
    print layers.get_output_shape(net['fwd1'], input_shapes=check)
    print layers.get_output_shape(net['dropout1'], input_shapes=check)
    print layers.get_output_shape(net['out'], input_shapes=check)
    return net

infile = sys.argv[1]  # training examples
embfile = sys.argv[2]    # character embeddings
vocabfile = sys.argv[3]  # character vocab
modelfile = sys.argv[4]
outfile = sys.argv[5]

# ch2ix = util.load_char_vocab(vocabfile)
ch2ix = cPickle.load(open(vocabfile, 'r'))
ix2ch = {ix: ch for ch, ix in ch2ix.items()}


def load_chemb():
    ix2emb = {}
    with open(embfile, 'r') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            assert len(parts) == 2
            ix = int(parts[0])
            assert ix in ix2ch
            vec = np.asarray(map(float, parts[1].split(' ')))
            ix2emb[ix] = vec.astype(theano.config.floatX)
    return ix2emb

ix2emb = load_chemb()
embedding_dim = ix2emb.values()[0].shape[0]
print 'embedding_dim:', embedding_dim

def load_data():

    def padmask(x, maxlen):
        n = x.shape[0]
        xpad = np.zeros((maxlen, embedding_dim))
        xmask = np.zeros((maxlen,))
        if n < maxlen:
            xpad[:n, :] = x
            xmask[:n] = 1
        else:
            xpad[:, :] = x
            xmask[:] = 1
        return xpad, xmask

    def readfile(fname):
        X, Y = [], []
        S = []

        labelfreq = {0: 0, 1: 0}
        errfreq = {0: 0, 1: 0}

        N = 0
        errors = 0
        total = 0

        with codecs.open(fname, 'r', 'utf8') as fr:
            for line in fr.readlines():
                line = line.rstrip('\n')
                parts = line.split('\t')
                label = int(parts[0])
                if label < 0:
                    label = 0
                string = parts[1]

                err = False

                x = np.zeros((len(string), embedding_dim))
                for j, ch in enumerate(string):
                    try:
                        ix = ch2ix[ch]
                    except KeyError:
                        try:
                            uch = unicode(ch)
                            ix = ch2ix[uch]
                        except Exception:
                            err = True
                            continue
                    emb = ix2emb[ix]
                    x[j, :] = emb

                if not err:
                    S.append(string)
                    X.append(x)
                    Y.append(label)
                    N += 1
                else:
                    errors += 1
                    errfreq[label] += 1
                total += 1
                labelfreq[label] += 1

        maxlen = -1
        for x in X:
            if x.shape[0] > maxlen:
                maxlen = x.shape[0]
        Xpad = np.zeros((N, maxlen, embedding_dim), dtype='float32')
        Xmask = np.zeros((N, maxlen), dtype='int32')
        for j, x in enumerate(X):
            xp, xm = padmask(x, maxlen)
            Xpad[j, :, :] = xp
            Xmask[j, :] = xm
        return Xpad, Xmask, np.asarray(Y).astype('int32'), S, maxlen

    xtrain, xmtrain, ytrain, train_strings, maxlen1 = readfile(infile)
    train = (xtrain, xmtrain, ytrain, train_strings)

    return train, maxlen1

dtrain, maxlen = load_data()

xvar = T.tensor3()
maskvar = T.imatrix()
yvar = T.ivector()

net = build_network(maxlen, embedding_dim, invar=xvar, maskvar=maskvar)

params = cPickle.load(open(modelfile, 'r'))
lasagne.layers.set_all_param_values(net.values(), params)

val_output = layers.get_output(net['out'], deterministic=True)
val_cost = lasagne.objectives.categorical_crossentropy(val_output, yvar).mean()
val_predictions = T.argmax(val_output, axis=1)
val_accuracy = T.mean(T.eq(val_predictions, yvar))
emb_out = layers.get_output(net['fwd1'])

print 'compiling f_eval'
f_eval = theano.function([xvar, maskvar, yvar],
                         [val_cost, val_accuracy, val_predictions, emb_out],
                         allow_input_downcast=True)

xin, xmin, yin, xstr = dtrain
N = xin.shape[0]
nbatches = N//BATCHSIZE
ixes = np.arange(N)

emo2emb = {}

curr = 0
j = 0
for batch in xrange(nbatches):
    bix = ixes[curr:curr+BATCHSIZE]
    curr += BATCHSIZE
    xmini = xin[bix, :, :].astype(theano.config.floatX)
    xmini_mask = xmin[bix, :]
    ymini = yin[bix]
    loss, acc, preds, embs = f_eval(xmini, xmini_mask, ymini)
    for i in range(len(bix)):
        pred = preds[i]
        truth = ymini[i]
        s = xstr[j]
        if pred == 1:
            if s not in emo2emb:
                emo2emb[s] = []
            emo2emb[s].append(embs[i])
        j += 1


writer = codecs.open(outfile, 'w', 'utf8')

for emo, embs in emo2emb.items():
    if len(embs) > 1:
        n = len(embs)
        d = embs[0].shape[0]
        m = np.zeros((n, d))
        for i, emb in enumerate(embs):
            m[i, :] = emb
        avg = np.mean(m, axis=0)
        s = ' '.join(['%.6f' % v for v in avg])
        line = '%s\t%s\n' % (emo, s)
        writer.write(line)
    else:
        s = ' '.join(['%.6f' % v for v in embs[0]])
        line = '%s\t%s\n' % (emo, s)
        writer.write(line)

writer.close()





