import numpy as np
import codecs


def load_labeled_file(filename, vocab_size, maxlen):

    def crop(prev, nxt):
        nprev = []
        nnext = []
        total = 0
        pix = len(prev) - 1
        nix = 0
        while total < maxlen:
            if pix >= 0:
                p = prev[pix]
                pix -= 1
                nprev.append(p)
                total += 1
            if nix < len(nxt):
                n = nxt[nix]
                nix += 1
                nnext.append(n)
                total += 1
            if pix < 0 and nix >= len(nxt):
                break
        nprev.reverse()
        return nprev, nnext

    raw = []
    with codecs.open(filename, 'r', 'utf8') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            raw.append(line)
    nlines = len(raw)
    ydata = np.zeros((nlines,), dtype='int32')
    prev_inputs = np.zeros((nlines, maxlen, vocab_size), dtype='int32')
    prev_mask_inputs = np.zeros((nlines, maxlen), dtype='int32')
    next_inputs = np.zeros_like(prev_inputs, dtype='int32')
    next_mask_inputs = np.zeros_like(prev_mask_inputs, dtype='int32')

    for i, line in enumerate(raw):
        parts = line.split('\t')
        label = int(parts[0])
        ydata[i] = label
        prev = parts[1]
        if len(prev) > 0:
            prev = map(int, prev.split('|'))
        else:
            prev = []
        nxt = parts[2]
        if len(nxt) > 0:
            nxt = map(int, nxt.split('|'))
        else:
            nxt = []

        prev, nxt = crop(prev, nxt)
        for j in xrange(len(prev)):
            prev_inputs[i, j, prev[j]] = 1
            prev_mask_inputs[i, j] = 1
        for j in xrange(len(nxt)):
            next_inputs[i, j, nxt[j]] = 1
            next_mask_inputs[i, j] = 1
        
        if (i % 500) == 0:
            print 'loaded %d/%d lines' % (i, nlines)

    return prev_inputs, prev_mask_inputs, next_inputs, next_mask_inputs, ydata


def chemb_from_file(infile, ch2ix):
    ix2ch = {ix: ch for ch, ix in ch2ix.items()}
    ix2emb = {}
    with open(infile, 'r') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            ix = int(parts[0])
            assert ix in ix2ch
            emb = np.asarray(map(float, parts[1].split(' ')))
            # emb = np.fromstring(parts[1], dtype='float32')
            ix2emb[ix] = emb
    return ix2emb


def char_vocab_from_file(vfile):
    ch2ix = {}
    with open(vfile, 'r') as f:
        for line in f.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            codepoint = int(parts[0])
            ix = int(parts[1])
            ch = unichr(codepoint)
            ch2ix[ch] = ix
    return ch2ix


def emos_from_file(emofile):
    emo2ix = {}
    with codecs.open(emofile, 'r', 'utf8') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            ix = int(parts[0])
            emo = parts[1]
            emo2ix[emo] = ix
    return emo2ix



