from patterns import patterns
from data_io import util, preprocess
import load_utils
import sys
import re
import codecs


# def load_emo_vocab(infile):
#     emo2ix = {}
#     with codecs.open(infile, 'r', 'utf8') as fr:
#         for line in fr.readlines():
#             line = line.rstrip('\n')
#             parts = line.split('\t')
#             eix = int(parts[0])
#             emo = parts[2]
#             emo2ix[emo] = eix
#     return emo2ix


def read_emoji_table(table_file):
    """
    http://stats.seandolinar.com/wp-content/uploads/2015/04/emoji_table.txt
    :param table_file:
    :return:
    """
    emoji = []
    ix = 0
    with codecs.open(table_file, 'r', 'utf8') as fr:
        for line in fr.readlines():
            if ix == 0:
                ix += 1
                continue
            line = line.rstrip('\n')
            parts = line.split(',')
            emo = parts[0]
            emoji.append(emo)
            ix += 1
            if ix >= 59:
                break
    return emoji


def sort_by_len(mspans):
    """
    Sort matched by length, descending order
    :param mspans: (start, end) spans found by regex
    :return: span list sorted by length
    """
    def mlen(m):
        return m.end('emo') - m.start('emo')
    return sorted(mspans, key=lambda m: mlen(m), reverse=True)


def overlaps(s, e, label):
    return sum(label[s:e]) > 0


def trim_contexts(ctxs, maxlen):
    contexts = []
    for c in ctxs:
        if len(c) <= maxlen:
            contexts.append(c)
        else:
            contexts.append(c[:maxlen])
    return contexts


def get_spans(line, emo_patterns):
    orig_line = line
    emoset = set([])
    # line = unicode(line)
    all_matches = []
    for p in emo_patterns:
        for m in re.finditer(p, line):
            all_matches.append(m)
    if len(all_matches) == 0:
        return [], emoset
    filled_positions = [0] * len(line)
    spans = []
    # sort matches by length
    all_matches = sort_by_len(all_matches)
    for m in all_matches:
        s = m.start('emo')
        e = m.end('emo')
        if not overlaps(s, e, filled_positions):
            for i in xrange(s, e):
                filled_positions[i] = 1
            spans.append((s, e))
            emoset.add(orig_line[s:e])
    return spans, emoset


def process(infile, vocab_file, emoji_table_file=None, emo_vocab_file=None):
    """
    Label file. Examples:

    Original line: "I think I just saw @RoniMakher's twin! 8)"
    Labeled line(s):
    "1\t8)\tI think I just saw @RoniMakher's twin! _"

    Original line: "omg i love JB :) but i missed his concert :'("
    Labeled line(s):
    "1\t:)\tomg i love JB _ but i missed his concert :'("
    "1\t:'(\tomg i love JB :) but i missed his concert _"

    :param infile: tweets with line information preserved
    :param emoji_table_file: list of relevant unicode codepoints (see read_emoji_table)
    :param vocab_file: character vocab (ch2ix)
    :param emo_vocab_file: pre-build dict emo2eix
    :return: void
    """
    outfile = infile + '.lab'

    if emoji_table_file is not None:
        emojis = read_emoji_table(emoji_table_file)
        for emoji in emojis:
            pstring = '(?P<emo>%s)' % emoji
            pattern = re.compile(pstring, flags=re.UNICODE)
            patterns.append(pattern)

    emoset = set([])
    match_results = []
    with codecs.open(infile, 'r', 'utf8') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            spans, emos = get_spans(line, patterns)
            emoset.update(emos)
            match_results.append((line, spans))

    ch2ix = util.load_char_vocab(vocab_file)
    if emo_vocab_file is None:
        emoset = list(emoset)
        emo2eix = {emo: eix for eix, emo in enumerate(emoset)}
        with codecs.open(outfile + '.vocab_emo', 'w', 'utf8') as fw:
            for emo, eix in emo2eix.items():
                line = '%d\t%s\n' % (eix, emo)
                fw.write(line)
    else:
        emo2eix = load_utils.emos_from_file(emo_vocab_file)

    # writer = codecs.open(outfile, 'w', 'utf8')
    writer = open(outfile, 'w')
    for line, spans in match_results:
        if len(spans) == 0:
            # output = '%d\t%s\n' % (0, line)
            # writer.write(output)
            continue
        # filter spans
        spans = filter(lambda (s, e): line[s:e] in emo2eix, spans)
        if len(spans) == 0:
            continue
        for start, end in spans:
            emo = line[start:end]
            previx = [ch2ix[ch] for ch in line[:start]]
            prev_str = '|'.join([str(v) for v in previx])
            nextix = [ch2ix[ch] for ch in line[end:]]
            next_str = '|'.join([str(v) for v in nextix])
            newline = '%d\t%s\t%s\n' % (emo2eix[emo], prev_str, next_str)
            writer.write(newline)
    writer.close()
