import argparse
from charemb import write_char_embeddings


def write_emb(args):
    write_char_embeddings.write_embeddings(args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model', type=str, help='path to saved model')
    parser.add_argument('vocab', type=str, help='ch2ix filename')
    parser.add_argument('outfile', type=str, help='filename to write embeddings to')
    parser.add_argument('--bidir', type=int, default=0, help='if 1, LSTM will be bidirectional')
    parser.add_argument('--nlayers', type=int, default=2, help='number of LSTM layers')
    parser.add_argument('--nrec', type=int, default=64, help='number of LSTM units')
    parser.add_argument('--seqlen', type=int, default=100, help='number of time steps to unroll the model')

    # parser.add_argument('--dropout', type=float, default=0.1, help='dropout proportion')
    # parser.add_argument('--batch-size', type=int, default=100, help='batch size')
    # parser.add_argument('--learning-rate', type=float, default=2e-3, help='learning rate')
    # parser.add_argument('--decay', type=float, default=0.95, help='learning rate decay')
    # parser.add_argument('--no-decay-epochs', type=int, default=10, help='run this many epochs before first decay')
    # parser.add_argument('--num-epochs', type=int, default=50, help='run this many epochs')
    # parser.add_argument('--val-freq', type=int, default=1000, help='validate every valfreq batches')
    # parser.add_argument('--early-stop', type=int, default=5, help='stop after no improvement for this many epochs')

    args = parser.parse_args()
    print args
    write_emb(args)



