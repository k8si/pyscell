

def load_char_vocab(vocab_file):
    ch2ix = {}
    with open(vocab_file, 'r', encoding='utf8', newline='\n') as fr:
        for line in fr.readlines():
            parts = line.split('\t')
            codepoint = int(parts[0])
            ix = int(parts[1])
            ch = chr(codepoint)
            ch2ix[ch] = ix
    return ch2ix
