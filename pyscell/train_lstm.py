import argparse
from charemb import train_lstm


def train(args):
    train_lstm.train_model(args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('train', type=str, help='train filename')
    parser.add_argument('val', type=str, help='validation filename')
    parser.add_argument('test', type=str, help='test filename')
    parser.add_argument('vocab', type=str, help='ch2ix filename')
    parser.add_argument('--model-file', type=str, default='model.pkl', help='where to save the model')
    parser.add_argument('--load-model', type=int, default=0, help='load the model from model_file?')

    """ model options """
    parser.add_argument('--bidir', type=int, default=0, help='if 1, LSTM will be bidirectional')
    parser.add_argument('--dropout', type=float, default=0.1, help='dropout proportion')
    parser.add_argument('--nlayers', type=int, default=2, help='number of LSTM layers')
    parser.add_argument('--nrec', type=int, default=64, help='number of LSTM units')
    parser.add_argument('--seqlen', type=int, default=100, help='number of time steps to unroll the model')

    """ optimization options """
    parser.add_argument('--batch-size', type=int, default=100, help='batch size')
    parser.add_argument('--decay', type=float, default=0.95, help='learning rate decay')
    parser.add_argument('--early-stop', type=int, default=5, help='stop after no improvement for this many epochs')
    parser.add_argument('--learning-rate', type=float, default=2e-3, help='learning rate')
    parser.add_argument('--no-decay-epochs', type=int, default=10, help='run this many epochs before first decay')
    parser.add_argument('--num-epochs', type=int, default=50, help='run this many epochs')
    parser.add_argument('--val-freq', type=int, default=1000, help='validate every valfreq batches')

    args = parser.parse_args()
    train(args)



