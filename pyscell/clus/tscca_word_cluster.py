from sklearn.cluster import KMeans
import numpy as np

def load_tscca(infile):
    word2ix = {}
    embs = []
    ix = 0
    with open(infile, 'r') as fr:
        for line in fr:
            line = line.rstrip('\n')
            parts = line.split(' ')
            word = parts[0]
            rest = parts[1:]
            vec = map(float, rest)
            word2ix[word] = ix
            embs.append(vec)
            ix += 1
            # if ix >= 5000:
            #     break
    return word2ix, embs




def cluster(infile, nclusters):
    word2ix, embs = load_tscca(infile)
    ix2word = {ix: word for word, ix in word2ix.items()}
    n = len(embs)
    d = len(embs[0])
    M = np.zeros((n, d))
    for i, emb in enumerate(embs):
        M[i, :] = emb
    ntrain = int(0.7 * n)
    train = M[:ntrain, :]
    test = M[(ntrain+1):, :]
    test_ixes = np.arange(ntrain+1, n)
    kmeans = KMeans(n_clusters=nclusters)
    kmeans.fit(train)
    preds = kmeans.predict(test)
    for cid in xrange(nclusters):
        print 'cluster', cid
        ixes = test_ixes[preds == cid]
        words = ' , '.join([ix2word[ix] for ix in ixes])
        print words
        print ''
    return None


filename = '/Users/kate/Dropbox/pyscell/rcv1.tscca.100k.200.c2'
cluster(filename, 20)
