import codecs
import numpy as np
import sys
from sklearn.cluster import KMeans


def loadit(infile):
    s = []
    e = []
    with codecs.open(infile, 'r', 'utf8') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            emo = parts[0]
            vals = parts[1]
            vals = map(float, vals.split(' '))
            s.append(emo)
            e.append(vals)
    N = len(s)
    D = len(e[0])
    M = np.zeros((N, D))
    for i in xrange(N):
        M[i, :] = e[i]
    return s, M

infile = sys.argv[1]
strings, embeddings = loadit(infile)

N = embeddings.shape[0]
nclus = 100
kmeans = KMeans(n_clusters=nclus)
kmeans.fit(embeddings)
preds = kmeans.predict(embeddings)
probs = kmeans.transform(embeddings)

ixes = np.arange(N)


clusters = {}
for i in xrange(nclus):
    cix = ixes[preds == i]
    emos = [strings[j] for j in cix]
    clusters[i] = emos


exemps = {}
for cid in clusters.keys():
    bestix = 0
    score = 10000.
    for i in xrange(N):
        prob = probs[i]
        z = np.sum(prob)
        prob /= z
        if np.argmin(prob) == cid:
            if prob[cid] < score:
                bestix = i
                score = prob[cid]
    exemps[cid] = strings[bestix]





writer = codecs.open('clusters.html', 'w', 'utf8')
for cid, emos in clusters.items():
    ex = exemps[cid]
    assert ex in emos
    writer.write('===> CLUSTER %d %s<br>\n' % (cid, ex)) #, exemps[cid]))
    for e in emos:
        writer.write('  %s  <br>\n' % e)
    writer.write('<br><br><br>\n\n\n')
writer.close()



