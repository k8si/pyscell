import numpy as np
from sklearn.cluster import KMeans
from data_io import util


def load_char_embeddings(embfile, vocabfile):
    ch2ix = util.load_char_vocab(vocabfile)
    ix2ch = {ix: ch for ch, ix in ch2ix.items()}
    ch2emb = {}
    with open(embfile, 'r') as fr:
        for line in fr.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            ix = int(parts[0])
            ch = ix2ch[ix]
            rest = parts[1]
            vec = np.asarray(map(float, rest.split(' ')))
            ch2emb[ch] = vec
    return ch2emb


def lookup(ch, ch2ix, ix2emb):
    ix = ch2ix[ch]
    emb = ix2emb[ix]
    return emb


def load_tscca(infile):
    word2emb = {}
    ix = 0
    with open(infile, 'r') as fr:
        for line in fr:
            line = line.rstrip('\n')
            parts = line.split(' ')
            word = parts[0]
            rest = parts[1:]
            vec = np.asarray(map(float, rest))
            word2emb[word] = vec
            ix += 1
            if ix >= 20000:
                break
    return word2emb


def do_cluster(M, nclusters, ix2word):
    n = M.shape[0]
    ntrain = int(0.7 * n)
    train = M[:ntrain, :]
    test = M[(ntrain+1):, :]
    test_ixes = np.arange(ntrain+1, n)
    kmeans = KMeans(n_clusters=nclusters, max_iter=1000)
    kmeans.fit(train)
    preds = kmeans.predict(test)
    probs = kmeans.transform(test)
    clusters = {}
    cluster_exemplars = {}
    for cid in xrange(nclusters):
        ixes = test_ixes[preds == cid]
        ps = probs[preds == cid]
        Z = 0.
        for prob in ps:
            Z += prob[cid]
        maxprob = -1000.
        exemp = None
        for prob, ix in zip(ps, ixes):
            p = prob[cid]/Z
            if p > maxprob:
                maxprob = p
                exemp = ix2word[ix]
        cluster_exemplars[cid] = exemp
        words = [ix2word[ix] for ix in ixes]
        assert exemp in words
        clusters[cid] = words
    return clusters, cluster_exemplars


def cluster(tscca_file, emb_file, vocab_file, nclusters):

    ch2emb = load_char_embeddings(emb_file, vocab_file)

    def word_in_vocab(word):
        found = True
        for ch in word:
            if ch not in ch2emb:
                found = False
                break
        return found

    D_char = ch2emb.values()[0].shape[0]

    word2emb_pre = load_tscca(tscca_file)
    word2emb_tscca = {}
    word2ix = {}
    ix = 0
    for word, emb in word2emb_pre.items():
        if word_in_vocab(word):
            word2emb_tscca[word] = emb
            word2ix[word] = ix
            ix += 1


    D_tscca = word2emb_tscca.values()[0].shape[0]

    word2emb_char = {}
    for word in word2emb_tscca.keys():
        W = np.zeros((len(word), D_char))
        for i, ch in enumerate(word):
            ch_emb = ch2emb[ch]
            W[i, :] = ch_emb
        avg = np.mean(W, axis=0)
        word2emb_char[word] = avg

    ix2word = {ix: word for word, ix in word2ix.items()}

    N = len(word2emb_tscca)
    M = np.zeros((N, D_tscca))
    for word, emb in word2emb_tscca.items():
        ix = word2ix[word]
        M[ix, :] = emb
    clus_tscca, exemplars_tscca = do_cluster(M, nclusters, ix2word)
    word2clus_tscca = {}
    for cid, words in clus_tscca.items():
        for w in words:
            word2clus_tscca[w] = cid

    for cid, exemplar in exemplars_tscca.items():
        print '===> CLUSTER', cid, 'EXEMPLAR', exemplar
        cwords = set(clus_tscca[cid])
        assert exemplar in cwords
        print ' '.join(clus_tscca[cid])
        print ''
        print ''

    N = len(word2emb_char)
    M = np.zeros((N, D_char))
    for word, emb in word2emb_char.items():
        ix = word2ix[word]
        M[ix, :] = emb
    clus_char, exemplars_char = do_cluster(M, nclusters, ix2word)
    word2clus_char = {}
    for cid, words in clus_char.items():
        for w in words:
            assert w not in word2clus_char
            word2clus_char[w] = cid

    for cid, exemplar in exemplars_char.items():
        print '===> CLUSTER', cid, 'EXEMPLAR', exemplar
        cwords = set(clus_char[cid])
        assert exemplar in cwords
        print ' '.join(clus_char[cid])
        print ''
        print ''



tscca_file = '/Users/kate/Dropbox/pyscell/rcv1.tscca.100k.200.c2'
emb_file = '/Users/kate/Dropbox/pyscell/ptb.embeddings'
vocab_file = '/Users/kate/Dropbox/pyscell/ptb/vocab.txt'
nclusters = 20
cluster(tscca_file, emb_file, vocab_file, nclusters)
