from emoemb import labeler
import argparse

if __name__ == '__main__':
    p = argparse.ArgumentParser()

    p.add_argument('infile', type=str)
    p.add_argument('chvocab', type=str)
    p.add_argument('emotable', type=str, default=None)
    p.add_argument('--emo-vocab', type=str, default=None)
    args = p.parse_args()

    labeler.process(args.infile,
                    args.chvocab,
                    emoji_table_file=args.emotable,
                    emo_vocab_file=args.emo_vocab)




