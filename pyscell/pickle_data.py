import argparse
from emoemb import pickle_data


def train(args):
    pickle_data.pickle_data(args)
    # pickle_data.test_pickle_data(args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=str, help='train filename')
    parser.add_argument('outfile', type=str, help='filename to write to')

    """ chemb model spec """
    parser.add_argument('--ch-vocab-size', type=int, help='char vocab size')
    parser.add_argument('--ch-model-file', type=str, help='ch emb model file')
    parser.add_argument('--ch-bidir', type=int, help='if 1, LSTM will be bidirectional')
    parser.add_argument('--ch-dropout', type=float, help='dropout proportion')
    parser.add_argument('--ch-nlayers', type=int, help='number of LSTM layers')
    parser.add_argument('--ch-nrec', type=int, help='number of LSTM units')
    parser.add_argument('--ch-seqlen', type=int, help='number of time steps to unroll the model')

    args = parser.parse_args()
    train(args)




