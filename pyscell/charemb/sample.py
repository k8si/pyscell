from lstm_network import build_network
import numpy as np
import theano
from theano import tensor as T
import lasagne
import cPickle
from data_io import util, txt2oh


RNG = np.random.RandomState(1234)


def sample(args):
    vocabfile = args.vocab
    modelfile = args.model_file
    load_model = bool(args.load_model)

    BIDIR = bool(args.bidir)
    DROPOUT_FRAC = args.dropout
    LSTM_LAYERS = args.nlayers
    LSTM_UNITS = args.nrec
    MODEL_SEQ_LEN = args.seqlen

    BATCH_SIZE = args.batch_size

    ch2ix = util.load_char_vocab(vocabfile)
    ix2ch = {ix: ch for ch, ix in ch2ix.items()}
    vocab_size = len(ch2ix)

    """ set up the variables and network """
    xvar = T.tensor3()
    xmaskvar = T.imatrix()
    # yvar = T.imatrix()

    net = build_network(vocab_size,
                        bidir=BIDIR,
                        dropout_frac=DROPOUT_FRAC,
                        lstm_units=LSTM_UNITS,
                        nlayers=LSTM_LAYERS,
                        seqlen=MODEL_SEQ_LEN,
                        batch_size=1,
                        invar=xvar,
                        maskvar=xmaskvar)

    all_params = cPickle.load(open(modelfile, 'r'))
    lasagne.layers.set_all_param_values(net.values(), all_params)

    output = lasagne.layers.get_output(net['out'], deterministic=True)

    print 'compiling fxn'
    fxn = theano.function([xvar, xmaskvar], output, allow_input_downcast=True)

    def gen_text(seed_str):
        # seed_str = seed_str.encode('utf8')
        x_ixes = [ch2ix[ch] for ch in seed_str]
        n = len(x_ixes)
        while n < MODEL_SEQ_LEN:
            x_ohs = np.zeros((1, MODEL_SEQ_LEN, vocab_size), dtype='int32')
            x_mask = np.zeros((1, MODEL_SEQ_LEN), dtype='int32')
            for j in xrange(n):
                ix = x_ixes[j]
                x_ohs[:, j, ix] = 1
                x_mask[:, j] = 1
            probs = fxn(x_ohs, x_mask) # 1 x seqlen x vocab_size
            probs = probs.reshape((MODEL_SEQ_LEN, vocab_size))
            target = probs[n-1, :]
            # chix = np.argmax(target)
            choices = np.arange(vocab_size)
            chix = np.random.choice(choices, p=target)
            # last5 = ''.join([ix2ch[ix] for ix in x_ixes[-6:]])
            # curr = ix2ch[chix]
            # print n, last5, '-->', curr
            x_ixes.append(chix)
            n = len(x_ixes)

        string = ''.join([ix2ch[ix] for ix in x_ixes])
        return string

    print 'type something'
    print '>',
    txt = raw_input()
    while txt is not None:
        txt = txt.decode('utf8')
        # print txt
        print gen_text(txt)
        print '>',
        txt = raw_input()

    # gend = gen_text('justin bieber')
    # print len(gend)
    # print gend

        # gen_len = len(x_ixes)
        # start = 0
        # for i in xrange(100):
        #     n = len(x_ixes[start:])
        #     if n > MODEL_SEQ_LEN:
        #         start = n - MODEL_SEQ_LEN
        #         n = len(x_ixes[start:])
        #     inp = x_ixes[start:]
        #     assert len(inp) <= MODEL_SEQ_LEN
        #     x_ohs = np.zeros((1, MODEL_SEQ_LEN, vocab_size), dtype='int32')
        #     x_mask = np.zeros((1, MODEL_SEQ_LEN), dtype='int32')
        #     for j in xrange(n):
        #         ix = x_ixes[j]
        #         x_ohs[:, j, ix] = 1
        #         x_mask[:, j] = 1
        #     probs = fxn(x_ohs, x_mask) # 1 x seqlen x vocab_size
        #     probs = probs.reshape((MODEL_SEQ_LEN, vocab_size))
        #     target = probs[n-1, :]
        #     choices = np.arange(vocab_size)
        #     chix = np.random.choice(choices, p=target)
        #     x_ixes.append(chix)






    # print 'type something'
    # print '>',
    # txt = raw_input()
    # while txt is not None:
    #     print txt
    #     n = len(txt)
        # oh = np.zeros((1, MODEL_SEQ_LEN, vocab_size), dtype='int32')
        # mask = np.zeros((1, MODEL_SEQ_LEN))
        # for i in xrange(n):
        #     ch = txt[i]
        #     chix = ch2ix[ch]
        #     oh[:, i, chix] = 1
        #     mask[:, i] = 1
        # probs = fxn(oh, mask)  # 1 x maxlen x vocab_size
        # probs = probs.reshape((MODEL_SEQ_LEN, vocab_size))
        # chixes = []
        # for i in xrange(n):
        #     ps = probs[i, :]
        #     choices = np.arange(vocab_size)
        #     chix = np.random.choice(choices, p=ps)
        #     chixes.append(chix)
        # string = ''.join([ix2ch[ix] for ix in chixes])
        # print string
        # print '>',
        # txt = raw_input()









