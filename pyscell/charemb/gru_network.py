# import lasagne
# from lasagne.layers import InputLayer, EmbeddingLayer, DropoutLayer, GRULayer, Gate, ReshapeLayer, DenseLayer, get_output
# from collections import OrderedDict
#
#
# BATCH_SIZE = 100                     # batch size
# MODEL_SEQ_LEN = 10                   # how many steps to unroll
# INI = lasagne.init.Uniform(0.1)      # initial parameter values
# REC_NUM_UNITS = 64                   # number of LSTM units
# DROPOUT_FRAC = 0.1                   # optional recurrent dropout
#
#
# def build_network(vocab_size, embedding_size, invar=None, hid1_initvar=None, hid2_initvar=None):
#     net = OrderedDict()
#
#     def create_gate():
#         return Gate(W_in=INI, W_hid=INI, W_cell=None)
#
#     net['input'] = InputLayer((BATCH_SIZE, MODEL_SEQ_LEN), input_var=invar)
#     net['emb'] = EmbeddingLayer(net['input'], input_size=vocab_size, output_size=embedding_size, W=INI)
#     net['dropout0'] = DropoutLayer(net['emb'], p=DROPOUT_FRAC)
#     net['rec1'] = GRULayer(net['dropout0'],
#                            num_units=REC_NUM_UNITS,
#                            resetgate=create_gate(),
#                            updategate=create_gate(),
#                            hidden_update=create_gate(),
#                            learn_init=False,
#                            hid_init=hid1_initvar)
#     net['dropout1'] = DropoutLayer(net['rec1'], p=DROPOUT_FRAC)
#     net['rec2'] = GRULayer(net['dropout1'],
#                            num_units=REC_NUM_UNITS,
#                            resetgate=create_gate(),
#                            updategate=create_gate(),
#                            hidden_update=create_gate(),
#                            learn_init=False,
#                            hid_init=hid2_initvar)
#     net['dropout2'] = DropoutLayer(net['rec2'], p=DROPOUT_FRAC)
#     net['reshape'] = ReshapeLayer(net['dropout2'], (BATCH_SIZE*MODEL_SEQ_LEN, REC_NUM_UNITS))
#     net['output'] = DenseLayer(net['reshape'], num_units=vocab_size, nonlinearity=lasagne.nonlinearities.softmax)
#     net['output'] = ReshapeLayer(net['output'], (BATCH_SIZE, MODEL_SEQ_LEN, vocab_size))
#     return net
