# from __future__ import division
# from data_io import util, txt2oh
# from gru_network import build_network, BATCH_SIZE, MODEL_SEQ_LEN, REC_NUM_UNITS
# import numpy as np
# import theano
# import theano.tensor as T
# import time
# import lasagne
# from lasagne.layers import get_output
# import cPickle
# import codecs
#
#
# def gru_embedder(trainfile, valfile, vocabfile):
#     EMBEDDING_SIZE = 20                 # Embedding size
#     TOL = 1e-6                          # numerical stability
#     LEARNING_RATE = 2e-3                # learning rate
#     DECAY = 0.95                        # decay factor
#     NO_DECAY_EPOCHS = 10                # run this many epochs before first decay
#     # max_grad_norm = 15                # scale steps if norm is above this value
#     NUM_EPOCHS = 50                     # Number of epochs to run
#     VAL_FREQ = 200                      # how often to do validation
#
#     buffsize = BATCH_SIZE * MODEL_SEQ_LEN + 1  # number of characters to read when getting data
#
#     def reorder(xin, batchsize, seqlen):
#         if xin.shape[0] % (batchsize * seqlen) == 0:
#             print('xin.shape[0] % (batchsize*seqlen) == 0 -> xin is set to xin = xin[:-1]')
#             xin = xin[:-1]
#         xresize = (xin.shape[0] // (batchsize*seqlen)) * seqlen * batchsize
#         nsamples = xresize // seqlen
#         nbatches = nsamples // batchsize
#         targets = xin[1:xresize+1].reshape(nsamples, seqlen)
#         xout = xin[:xresize].reshape(nsamples, seqlen)
#         out = np.zeros(nsamples, dtype=int)
#         for i in xrange(nbatches):
#             val = range(i, nbatches*batchsize+i, nbatches)
#             out[i*batchsize:(i+1)*batchsize] = val
#         xout = xout[out]
#         targets = targets[out]
#         return xout.astype('int32'), targets.astype('int32')
#
#     ch2ix = util.load_char_vocab(vocabfile)
#     vocab_size = len(ch2ix)
#     ix2ch = {ix: ch for ch, ix in ch2ix.items()}
#     traingen = txt2oh.from_file_ixes(trainfile, ch2ix, buffsize=buffsize)
#     valgen = txt2oh.from_file_ixes(valfile, ch2ix, buffsize=buffsize)
#
#     xvar = T.imatrix()
#     yvar = T.imatrix()
#     hid1_initvar = T.matrix()
#     hid2_initvar = T.matrix()
#
#     net = build_network(vocab_size, EMBEDDING_SIZE, invar=xvar, hid1_initvar=hid1_initvar, hid2_initvar=hid2_initvar)
#
#     def calc_cross_ent(net_output, targets):
#         preds = T.reshape(net_output, (BATCH_SIZE * MODEL_SEQ_LEN, vocab_size))
#         preds += TOL
#         targets = T.flatten(targets)
#         cost = T.nnet.categorical_crossentropy(preds, targets)
#         return cost
#
#     trainout, lrec1_hid_out, lrec2_hid_out = get_output([net['output'], net['rec1'], net['rec2']],
#                                                         xvar,
#                                                         deterministic=False)
#
#     hidden_states_train = [lrec1_hid_out, lrec2_hid_out]
#     evalout, lrec1_hid_out, lrec2_hid_out = get_output([net['output'], net['rec1'], net['rec2']],
#                                                        xvar,
#                                                        deterministic=True)
#     hidden_states_eval = [lrec1_hid_out, lrec2_hid_out]
#
#     cost_train = T.mean(calc_cross_ent(trainout, yvar))
#     cost_eval = T.mean(calc_cross_ent(evalout, yvar))
#
#     params = lasagne.layers.get_all_params(net.values(), trainable=True)
#     grads = T.grad(cost_train*MODEL_SEQ_LEN, params)
#     grads = [T.clip(g, -5, 5) for g in grads]
#     shared_rate = theano.shared(lasagne.utils.floatX(LEARNING_RATE))
#     grad_updates = lasagne.updates.adam(grads, params, learning_rate=shared_rate)
#
#     print 'compiling f_eval'
#     fxn_inp = [xvar, yvar, hid1_initvar, hid2_initvar]
#     f_eval = theano.function(fxn_inp,
#                              [cost_eval,
#                               hidden_states_eval[0][:, -1],
#                               hidden_states_eval[1][:, -1],
#                               evalout])
#
#     print 'compiling f_train'
#     f_train = theano.function(fxn_inp,
#                               [cost_train,
#                                hidden_states_train[0][:, -1],
#                                hidden_states_train[1][:, -1]],
#                               updates=grad_updates)
#
#     # def calc_perplexity(x, y):
#     #     nbatches = x.shape[0] // BATCH_SIZE
#     #     lcost = []
#     #     hid1, hid2 = [np.zeros((BATCH_SIZE, REC_NUM_UNITS), dtype='float32') for _ in xrange(2)]
#     #     for i in xrange(nbatches):
#     #         xbatch = x[i*BATCH_SIZE:(i+1)*BATCH_SIZE]
#     #         ybatch = y[i*BATCH_SIZE:(i+1)*BATCH_SIZE]
#     #         cost, hid1, hid2, guesses = f_eval(xbatch, ybatch, hid1, hid2)
#     #         lcost.append(cost)
#     #     nchars = (x.shape[0] - 1) / MODEL_SEQ_LEN
#     #     perplexity = np.exp(np.sum(lcost) / nchars)
#     #     return perplexity
#
#     thelog = codecs.open('log.txt', 'w+', 'utf8')
#
#     def do_evaluate(verbose=False, quick=False):
#         lcost = []
#         hid1, hid2 = [np.zeros((BATCH_SIZE, REC_NUM_UNITS), dtype='float32') for _ in xrange(2)]
#         nchars = 0
#         nbatches = 0
#         printct = 0
#         done = False
#         while not done:
#             try:
#                 chunk = next(valgen)
#             except StopIteration:
#                 done = True
#                 continue
#             xbatch, ybatch = reorder(chunk, BATCH_SIZE, MODEL_SEQ_LEN)
#             cost, hid1, hid2, guesses = f_eval(xbatch, ybatch, hid1, hid2)
#             lcost.append(cost)
#             nchars += len(chunk)
#             nbatches += 1
#             if verbose and printct < 10:
#                 probs = np.reshape(guesses, (BATCH_SIZE * MODEL_SEQ_LEN, vocab_size))
#                 # probs += TOL
#                 preds = probs.argmax(axis=1)
#                 targets = np.reshape(ybatch, (BATCH_SIZE * MODEL_SEQ_LEN,))
#                 pred_str = ''.join([ix2ch[ix] for ix in preds])
#                 targ_str = ''.join([ix2ch[ix] for ix in targets])
#                 thelog.write('pred: %s\n' % pred_str)
#                 thelog.write('true: %s\n' % targ_str)
#                 printct += 1
#             if quick:
#                 if nbatches > 25:
#                     done = True
#         # nchars = (nchars - 1)/MODEL_SEQ_LEN
#         nchars = nchars/MODEL_SEQ_LEN
#         perplexity = np.exp(np.sum(lcost) / nchars)
#         thelog.write('perplexity valid: %.6f' % perplexity)
#         thelog.flush()
#         return perplexity, nchars, nbatches
#
#     nbatches = 0
#     nchars = 0
#     epochs = 0
#     lcost, lnorm, batchtime = [], [], time.time()
#     hid1, hid2 = [np.zeros((BATCH_SIZE, REC_NUM_UNITS), dtype='float32') for _ in xrange(2)]
#
#     while epochs < NUM_EPOCHS:
#         try:
#             chunk = next(traingen)
#         except StopIteration:
#             print 'done with epoch', epochs
#             with open('model.pkl', 'wb') as f:
#                 cPickle.dump(lasagne.layers.get_all_param_values(net.values()), f, cPickle.HIGHEST_PROTOCOL)
#             if epochs > NO_DECAY_EPOCHS:
#                 current_rate = shared_rate.get_value()
#                 new_learning_rate = current_rate * 0.95
#                 shared_rate.set_value(lasagne.utils.floatX(new_learning_rate))
#                 # shared_rate.set_value(lasagne.utils.floatX(current_rate / float(DECAY)))
#             elapsed = time.time() - batchtime
#             mean_loss = np.mean(lcost)
#             words_per_sec = float(BATCH_SIZE * MODEL_SEQ_LEN * len(lcost)) / elapsed
#             # n_words_evaluated = (nchars - 1) / MODEL_SEQ_LEN
#             n_words_evaluated = nchars / MODEL_SEQ_LEN
#             perplexity_valid, chars_val, batches_val = do_evaluate()
#             perplexity_train = np.exp(np.sum(lcost) / n_words_evaluated)
#             thelog.write('epoch           : %d\n' % epochs)
#             thelog.write('mean loss       : %.5f\n' % mean_loss)
#             thelog.write('perplexity train: %.5f\n' % perplexity_train)
#             thelog.write('perplexity valid: %.5f\n' % perplexity_valid)
#             thelog.write('chars per second: %d\n' % words_per_sec)
#             thelog.flush()
#             epochs += 1
#             traingen = txt2oh.from_file_ixes(trainfile, ch2ix, buffsize=buffsize)
#             valgen = txt2oh.from_file_ixes(valfile, ch2ix, buffsize=buffsize)
#             lcost = []
#             hid1, hid2 = [np.zeros((BATCH_SIZE, REC_NUM_UNITS), dtype='float32') for _ in xrange(2)]
#             batchtime = time.time()
#             continue
#         xbatch, ybatch = reorder(chunk, BATCH_SIZE, MODEL_SEQ_LEN)
#         # cost, norm, hid1, hid2 = f_train(xbatch, ybatch, hid1, hid2)
#         cost, hid1, hid2 = f_train(xbatch, ybatch, hid1, hid2)
#         lcost.append(cost)
#         # lnorm.append(norm)
#         if nbatches > 0 and (nbatches % VAL_FREQ) == 0:
#             print 'epoch %d, batch %d' % (epochs, nbatches)
#             perplexity_valid, nchars_valid, nbatches_valid = do_evaluate(verbose=True, quick=True)
#             print 'perplexity valid:', perplexity_valid
#             print 'evaluated %d chars, %d batches' % (nchars_valid, nbatches_valid)
#             valgen = txt2oh.from_file_ixes(valfile, ch2ix, buffsize=buffsize)
#         nbatches += 1
#         nchars += len(chunk)
#
#     mean_loss = np.mean(lcost)
#     n_chars_evaluated = nchars/MODEL_SEQ_LEN
#     perplexity_train = np.exp(np.sum(lcost)/n_chars_evaluated)
#     thelog.write('ran %d epochs\n' % epochs)
#     thelog.write('loss (final): %.5f\n' % mean_loss)
#     thelog.write('perplexity train (final): %.5f\n' % perplexity_train)
#     thelog.close()
#
