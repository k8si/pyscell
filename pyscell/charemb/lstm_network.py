import lasagne
from lasagne import layers
from theano.tensor import add
from collections import OrderedDict


INI = lasagne.init.Uniform(0.1)      # initial parameter values


def tmean(a, b):
    return add(a, b) / 2.


def build_network(vocab_size,
                  bidir=False,
                  dropout_frac=0.0,
                  lstm_units=1,
                  nlayers=1,
                  seqlen=None,
                  grad_clip=5.,
                  batch_size=None,
                  invar=None,
                  maskvar=None):

    net = OrderedDict()
    net['inp'] = layers.InputLayer((None, None, vocab_size), input_var=invar)
    net['mask'] = layers.InputLayer((None, None), input_var=maskvar)

    gate_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        b=lasagne.init.Constant(0.)
    )
    cell_params = layers.recurrent.Gate(
        W_in=lasagne.init.Orthogonal(),
        W_hid=lasagne.init.Orthogonal(),
        W_cell=None,
        b=lasagne.init.Constant(0.),
        nonlinearity=lasagne.nonlinearities.tanh
    )

    prev_layer = net['inp']
    for i in range(nlayers):
        lfwd = layers.LSTMLayer(
            prev_layer,
            num_units=lstm_units,
            nonlinearity=lasagne.nonlinearities.tanh,
            ingate=gate_params,
            forgetgate=gate_params,
            cell=cell_params,
            outgate=gate_params,
            grad_clipping=grad_clip,
            mask_input=net['mask'],
            learn_init=True
        )
        fwd_layer_id = 'fwd_%d' % i
        net[fwd_layer_id] = lfwd

        if bidir:
            lbwd = layers.LSTMLayer(
                lfwd,
                num_units=lstm_units,
                nonlinearity=lasagne.nonlinearities.tanh,
                ingate=gate_params,
                forgetgate=gate_params,
                cell=cell_params,
                outgate=gate_params,
                grad_clipping=grad_clip,
                mask_input=net['mask'],
                learn_init=True,
                backwards=True
            )
            bwd_layer_id = 'bwd_%d' % i
            net[bwd_layer_id] = lbwd

            # lmerge = layers.ElemwiseMergeLayer([net[fwd_layer_id], net[bwd_layer_id]], tmean)
            lmerge = layers.ElemwiseSumLayer([lfwd, lbwd])
            mrg_layer_id = 'mrg_%d' % i
            net[mrg_layer_id] = lmerge
            prev_layer = net[mrg_layer_id]

        else:

            prev_layer = net[fwd_layer_id]

        ldropout = layers.DropoutLayer(prev_layer, p=dropout_frac)
        layer_id = 'drop_%d' % i
        net[layer_id] = ldropout
        prev_layer = ldropout

    # net['dropout1'] = layers.DropoutLayer(prev_layer, p=dropout_frac)
    net['reshape'] = layers.ReshapeLayer(prev_layer, (batch_size * seqlen, lstm_units))
    net['out'] = layers.DenseLayer(net['reshape'], num_units=vocab_size, nonlinearity=lasagne.nonlinearities.softmax)
    net['out'] = layers.ReshapeLayer(net['out'], (batch_size, seqlen, vocab_size))

    return net


