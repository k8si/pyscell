from __future__ import division
from .lstm_network import build_network
from utils import load_char_vocab
import numpy as np
import theano
import theano.tensor as T
import time
import lasagne
from lasagne.layers import get_output, get_all_params
import pickle
import codecs
import os


def shuffle_file(filename):
    if filename.endswith('.shuff'):
        cmd = 'shuf --output=%s %s' % (filename, filename)
        print(cmd)
        os.system(cmd)
    else:
        orig = filename[:]
        filename = filename + '.shuff'
        cmd = 'shuf --output=%s %s' % (filename, orig)
        print(cmd)
        os.system(cmd)
    return filename


def readlines(filename):
    with open(filename, 'r', encoding='utf8', newline='\n') as fr:
        for line in fr.readlines():
            yield line


def lines2oh(lines, maxlen, ch2ix):
    n = len(lines)
    vocab_size = len(ch2ix)
    yoh = np.zeros((n, maxlen, vocab_size))
    xoh = np.zeros((n, maxlen, vocab_size))
    mask = np.zeros((n, maxlen))
    for i, line in enumerate(lines):
        llen = len(line)
        x = line[0:llen-1]
        y = line[1:]
        assert len(x) == len(y)
        for j in range(len(x)):
            ch = x[j]
            chix = ch2ix[ch]
            xoh[i, j, chix] = 1
            mask[i, j] = 1
        for j in range(len(y)):
            ch = y[j]
            chix = ch2ix[ch]
            yoh[i, j, chix] = 1
    y = yoh.argmax(axis=2)
    return y, xoh, mask


def train_model(args):
    """
    Train an LSTM character-level language model
    :param args: args from an ArgumentParser
    :return: void
    """
    """ read the args """
    trainfile = args.train
    valfile = args.val
    testfile = args.test
    vocabfile = args.vocab
    modelfile = args.model_file
    load_model = bool(args.load_model)

    BIDIR = bool(args.bidir)
    DROPOUT_FRAC = args.dropout
    LSTM_LAYERS = args.nlayers
    LSTM_UNITS = args.nrec
    MODEL_SEQ_LEN = args.seqlen

    BATCH_SIZE = args.batch_size
    DECAY = args.decay
    LEARNING_RATE = args.learning_rate
    EARLY_STOPPING = args.early_stop
    NO_DECAY_EPOCHS = args.no_decay_epochs
    NUM_EPOCHS = args.num_epochs
    VAL_FREQ = args.val_freq

    buff_size = BATCH_SIZE * MODEL_SEQ_LEN + 1  # number of characters to read when getting data
    logger = codecs.open('log.txt', 'w', 'utf8')

    def logit(msg):
        print(msg)
        logger.write(msg + '\n')

    logit('THEANO VERSION : %s' % theano.__version__)
    logit('LASAGNE VERSION: %s' % lasagne.__version__)
    logit(str(args))

    ch2ix = load_char_vocab(vocabfile)
    ix2ch = {ix: ch for ch, ix in ch2ix.items()}
    vocab_size = len(ch2ix)
    logit('vocab size: %d' % vocab_size)

    """ data generators """
    traingen = readlines(trainfile)
    valgen = readlines(valfile)
    testgen = readlines(testfile)

    """ set up the variables and network """
    xvar = T.tensor3()
    xmaskvar = T.imatrix()
    yvar = T.imatrix()

    net = build_network(vocab_size,
                        bidir=BIDIR,
                        dropout_frac=DROPOUT_FRAC,
                        lstm_units=LSTM_UNITS,
                        nlayers=LSTM_LAYERS,
                        seqlen=MODEL_SEQ_LEN,
                        batch_size=BATCH_SIZE,
                        invar=xvar,
                        maskvar=xmaskvar)

    if load_model:
        all_params = pickle.load(open(modelfile, 'r'))
        lasagne.layers.set_all_param_values(net.values(), all_params)
        logit('loaded params from file %s' % modelfile)

    check = {net['inp']: (BATCH_SIZE, MODEL_SEQ_LEN, vocab_size)}
    for lid, layer in net.items():
        if lid.startswith('fwd_') or lid.startswith('bwd_') or lid.startswith('mrg_'):
            shape = lasagne.layers.get_output_shape(layer, input_shapes=check)
            shape_str = ','.join([str(v) for v in shape])
            logit('%s shape: (%s)' % (lid, shape_str))
    shape = lasagne.layers.get_output_shape(net['out'], input_shapes=check)
    shape_str = ','.join([str(v) for v in shape])
    logit('outp shape: (%s)' % shape_str)

    def compute_crossent(net_output, targets):
        """
        :param net_output: batchsize x seqlen x vocabsize
        :param targets: batchsize x seqlen
        """
        preds = T.reshape(net_output, (BATCH_SIZE * MODEL_SEQ_LEN, vocab_size))
        targets = T.flatten(targets)
        crossent = lasagne.objectives.categorical_crossentropy(preds, targets).mean()
        return crossent

    train_output = get_output(net['out'], deterministic=False)
    train_cost = compute_crossent(train_output, yvar)

    all_params = get_all_params(net.values())
    shared_rate = theano.shared(lasagne.utils.floatX(LEARNING_RATE))
    grad_updates = lasagne.updates.adam(train_cost, all_params, learning_rate=shared_rate)

    val_output = get_output(net['out'], deterministic=True)
    val_cost = compute_crossent(val_output, yvar)
    val_predictions = T.argmax(val_output, axis=2)

    print('compiling f_train')
    f_train = theano.function([xvar, xmaskvar, yvar], [train_cost], updates=grad_updates, allow_input_downcast=True)
    print('compiling f_eval')
    f_eval = theano.function([xvar, xmaskvar, yvar], [val_cost, val_predictions], allow_input_downcast=True)

    def get_batch(gen):
        lines = []
        count = 0
        while count < BATCH_SIZE:
            try:
                line = next(gen)
                lines.append(line)
                count += 1
            except StopIteration:
                return None, None, None
        ybatch, xbatch, mbatch = lines2oh(lines, MODEL_SEQ_LEN, ch2ix)
        return xbatch, mbatch, ybatch


    def evaluate(gen, verbose=False, quick=False):
        val_losses = []
        nbatches = 0
        do_print = 0
        while True:
            xbatch, mbatch, ybatch = get_batch(gen)
            if xbatch is None:
                break
            vloss, vpred = f_eval(xbatch, mbatch, ybatch)
            ybatch = ybatch.reshape((BATCH_SIZE * MODEL_SEQ_LEN,))
            vpred = vpred.reshape((BATCH_SIZE * MODEL_SEQ_LEN,))
            val_losses.append(vloss)
            nbatches += 1
            if verbose and do_print < 2:
                pred_str = ''.join([ix2ch[ix] for ix in vpred])
                true_str = ''.join([ix2ch[ix] for ix in ybatch])
                print('=====================================================')
                print('==> pred:')
                print(pred_str[:500])
                print('')
                print('==> true:')
                print(true_str[:500])
                print('=====================================================')
                do_print += 1
            if quick:
                if nbatches >= 20:
                    break
        return np.mean(val_losses)

    best_test_loss = 1000.
    no_decrease_epochs = 0

    logger.flush()

    """ training """

    for epoch in range(NUM_EPOCHS):
        logit('starting epoch %d' % epoch)
        start_time = time.time()
        losses = []
        nbatches_ = 0
        done = False
        while not done:
            xbatch_, mbatch_, ybatch_ = get_batch(traingen)
            if xbatch_ is None:
                done = True
                continue
            loss = f_train(xbatch_, mbatch_, ybatch_)
            losses.append(loss)
            if nbatches_ > 0 and (nbatches_ % VAL_FREQ) == 0:
                valstart = time.time()
                val_loss = evaluate(valgen, verbose=True, quick=True)
                valtime = time.time() - valstart
                logit('--- validation')
                logit('epoch          : %d' % epoch)
                logit('batch          : %d' % nbatches_)
                logit('took           : %d secs' % valtime)
                logit('loss (val)     : %.6f' % val_loss)
                logit('---')
                logger.flush()
                tmp_model = '%s.ep%dba%d_%.4f.chk' % (modelfile, epoch, nbatches_, val_loss)
                logit('saving model to %s' % tmp_model)
                with open(tmp_model, 'wb') as f:
                    pickle.dump(lasagne.layers.get_all_param_values(net.values()), f)
                logger.flush()
                valfile = shuffle_file(valfile)
                valgen = readlines(valfile)

            nbatches_ += 1

        epoch_time = time.time() - start_time
        val_loss = evaluate(valgen)
        test_loss = evaluate(testgen)
        if test_loss < best_test_loss:
            best_test_loss = test_loss
            no_decrease_epochs = 0
            logit('saving model to %s' % modelfile)
            with open(modelfile, 'wb') as f:
                pickle.dump(lasagne.layers.get_all_param_values(net.values()), f)
        else:
            no_decrease_epochs += 1

        logit('--- done with epoch')
        logit('epoch          : %d' % epoch)
        logit('# batches      : %d' % nbatches_)
        logit('took           : %d secs' % epoch_time)
        logit('loss (train)   : %.6f' % np.mean(losses))
        logit('loss (val)     : %.6f' % val_loss)
        logit('loss (test)    : %.6f' % test_loss)
        logit('loss (best)    : %.6f' % best_test_loss)
        logit('no improvements: %d epochs' % no_decrease_epochs)
        logit('---')

        trainfile = shuffle_file(trainfile)
        traingen = readlines(trainfile)
        valfile = shuffle_file(valfile)
        valgen = readlines(valfile)
        testfile = shuffle_file(testfile)
        testgen = readlines(testfile)

        if no_decrease_epochs > EARLY_STOPPING:
            logit('stopping after %d epochs' % epoch)
            break

        if epoch > NO_DECAY_EPOCHS:
            curr_rate = shared_rate.get_value()
            new_rate = curr_rate * DECAY
            shared_rate.set_value(lasagne.utils.floatX(new_rate))

        logger.flush()










