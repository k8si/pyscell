from __future__ import division
from data_io import util, txt2oh
from lstm_network import build_network
import numpy as np
import theano
import theano.tensor as T
import time
import lasagne
from lasagne.layers import get_output, get_all_params
import cPickle
import codecs


def write_embeddings(args):

    print 'THEANO VERSION', theano.__version__
    print 'LASAGNE VERSION', lasagne.__version__

    modelfile = args.model
    vocabfile = args.vocab
    outfile = args.outfile

    BIDIR = bool(args.bidir)
    MODEL_SEQ_LEN = args.seqlen
    LSTM_UNITS = args.nrec
    LSTM_LAYERS = args.nlayers
    # DROPOUT_FRAC = args.dropout

    ch2ix = util.load_char_vocab(vocabfile)
    ix2ch = {ix: ch for ch, ix in ch2ix.items()}
    vocab_size = len(ch2ix)

    xvar = T.tensor3()
    yvar = T.imatrix()

    net = build_network(vocab_size,
                        bidir=BIDIR,
                        dropout_frac=0.0,
                        lstm_units=LSTM_UNITS,
                        nlayers=LSTM_LAYERS,
                        seqlen=MODEL_SEQ_LEN,
                        batch_size=1,
                        invar=xvar)

    # net = build_network(vocab_size,
    #                     MODEL_SEQ_LEN,
    #                     LSTM_UNITS,
    #                     LSTM_LAYERS,
    #                     0.0,
    #                     batch_size=1,
    #                     invar=xvar)

    all_params = cPickle.load(open(modelfile, 'r'))
    lasagne.layers.set_all_param_values(net.values(), all_params)

    last_layer_id = 'fwd_%d' % (LSTM_LAYERS - 1)
    output = get_output(net[last_layer_id], xvar, deterministic=True)

    print 'compiling f_lookup'
    f_lookup = theano.function([xvar], output, allow_input_downcast=True)

    start = time.time()
    ct = 0
    writer = open(outfile, 'w')
    for ch, ix in ch2ix.items():
        oh = np.zeros((1, 1, vocab_size)).astype(theano.config.floatX)
        oh[:, 0, ix] = 1
        emb = f_lookup(oh).reshape((LSTM_UNITS,))
        emb_str = ' '.join(['%.6f' % v for v in emb])
        line = '%d\t%s\n' % (ix, emb_str)
        writer.write(line)
        ct += 1
        if (ct % 100) == 0:
            print 'done with %d/%d' % (ct, vocab_size)
    writer.close()
    final = time.time() - start
    print 'took', final, 'secs'



