import random
import numpy as np
import sys

pos1 = "1\t:)"
pos2 = "1\t(^_^)"
neg1 = "-1\taaa"
neg2 = "-1\tbb)"

n = int(sys.argv[1])

data = []
for i in xrange(n//4):
    data.append(pos1)
    data.append(pos2)
    data.append(neg1)
    data.append(neg2)

np.random.shuffle(data)
print '\n'.join(data)




# n = 1000
#
# freqs = {'a': 0, 'b': 0, 'c': 0}
# seq = ['a']
# prev = seq[-1]
# for i in xrange(n):
#     if prev == 'a':
#         next = np.random.choice(['a', 'b', 'c'], p=[0.8, 0.1, 0.1])
#     elif prev == 'b':
#         next = np.random.choice(['a', 'b', 'c'], p=[0.1, 0.8, 0.1])
#     else:
#         next = np.random.choice(['a', 'b', 'c'], p=[0.1, 0.1, 0.8])
#     seq.append(next)
#     prev = next
#
# print ''.join(seq)
# # a = ['abc']*n
# # # b = ['aaa']*(n//10)
# # # seqs = a + b
# # seqs = a
# # random.shuffle(seqs)
# # print ''.join(seqs)
