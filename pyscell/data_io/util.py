import codecs
import sys


ENCODING = 'utf8'


class VocabException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return 'VocabException: %s' % self.msg


def readlines(fname):
    with codecs.open(fname, 'r', ENCODING) as f:
        for line in f.readlines():
            line = line.rstrip('\n')
            yield line


def url_char():
    codepoint = 198
    ch = unichr(codepoint)
    return ch


def load_char_vocab(vfile):
    ch2ix = {}
    with open(vfile, 'r') as f:
        for line in f.readlines():
            line = line.rstrip('\n')
            parts = line.split('\t')
            codepoint = int(parts[0])
            ix = int(parts[1])
            ch = unichr(codepoint)
            ch2ix[ch] = ix
    return ch2ix


def find_replacement_char(ch2ix):
    """
    Find a codepoint that does not occur in charset (for use in substitutions).
    :param map from character to vocab index
    :return: tuple: (replacement char, modified ch2ix that includes replacement char)
    """
    chars = ch2ix.keys()
    codepoints = sorted(map(lambda ch: ord(ch), chars))
    found = -1
    prev = codepoints[0]
    j = 1
    while found < 0 and j < len(codepoints):
        cp = codepoints[j]
        if cp != (prev + 1):
            found = cp - 1
            break
        prev = cp
        j += 1
    assert found >= 0
    replace_char = unichr(found)
    assert replace_char not in chars
    ixes = sorted(ch2ix.values())
    maxix = ixes[-1]
    ch2ix[replace_char] = maxix+1
    print replace_char, found, maxix+1
    return replace_char, found, ch2ix


def test_load_vocab(infile):
    ch2ix = load_char_vocab(infile)
    print(len(ch2ix))


# def test():
#     import re
#     url = re.compile(ur'http[s]?://t.co/\w+')
#     fname = '/home/kate/Dropbox/emo-cluster/data/debug-url-replace.txt'
#     URL_CHAR = url_char()
#     # URL_CHAR = 'URL'
#     for line in readlines(fname):
#         print(line)
#         text = re.sub(url, URL_CHAR, line)
#         print(text)
#         print('')


if __name__ == '__main__':
    test_load_vocab(sys.argv[1])


