from util import url_char
from constants import ENCODING
import codecs
import sys


"""
char -> codepoint -> vocab_idx
"""


def make_char_vocab(infile):
    outfile = infile + '.vocab'
    data = codecs.open(infile, 'r', ENCODING).read()
    chars = set(data)
    if use_url_char:
        chars.add(url_char())
    chars = list(chars)
    char_to_ix = {ch: i for i, ch in enumerate(chars)}
    with open(outfile, 'w') as f:
        for ch in chars:
            uni = unicode(ch)
            codepoint = ord(uni)
            line = '%d\t%d\n' % (codepoint, char_to_ix[ch])
            f.write(line)


if __name__ == '__main__':
    infile = sys.argv[1]
    use_url_char = bool(int(sys.argv[2]))
    make_char_vocab(infile)


