import sys
import numpy as np


infile = sys.argv[1]
do_shuffle = int(sys.argv[2])

train_frac = 0.8

all = []

with open(infile, 'r', encoding='utf8', newline='\n') as fr:
    for line in fr.readlines():
        all.append(line.rstrip('\n'))

N = len(all)
ntrain = int(np.ceil(train_frac * N))
nrest = N - ntrain
nval = int(np.ceil(0.5 * nrest))
ntest = N - ntrain - nval
print(ntrain, nval, ntest, ntrain+nval+ntest, N)

ixes = np.arange(N)
if do_shuffle:
    np.random.shuffle(ixes)

train_ix = ixes[:ntrain]
val_ix = ixes[ntrain:ntrain+nval]
test_ix = ixes[ntrain+nval:]

assert (len(train_ix) + len(val_ix) + len(test_ix)) == N


def write_set(set_ixes, set_id):
    with open(infile + set_id, 'w', encoding='utf8', newline='\n') as fw:
        for ix in set_ixes:
            line = all[ix]
            fw.write(line + '\n')

write_set(train_ix, '.train')
write_set(val_ix, '.val')
write_set(test_ix, '.test')

