import re
import sys


URL_REPLACE = re.compile(r'http[s]?://t.c(o/\w+)?', flags=re.UNICODE)
NEWLINE_REPLACE = re.compile(r'([\n]+)|(([\\][n])+)', flags=re.UNICODE)
ESCQUO_REPLACE = re.compile(r'([\\]["])+', flags=re.UNICODE)
USERNAME_REPLACE = re.compile(r'[@]\S+')


def find_replacement_char(ch2ix):
    chars = ch2ix.keys()
    codepoints = [ord(ch) for ch in chars]
    srt = sorted(codepoints)
    curr = 128  #  avoid ascii and control chars
    while curr < len(codepoints) - 1:
        curr_cp = srt[curr]
        next_cp = srt[curr+1]
        if next_cp != (curr_cp + 1):
            break
        curr += 1
    replace_cp = srt[curr] + 1
    replace_ch = chr(replace_cp)
    return replace_cp, replace_ch


def clean_tweet(text, url_char):
    text = text.strip('"')
    # replace urls with url_char
    text = re.sub(URL_REPLACE, url_char, text)
    # strip stray newlines
    text = re.sub(NEWLINE_REPLACE, '', text)
    # strip escape quotes
    text = re.sub(ESCQUO_REPLACE, '"', text)
    text = re.sub(USERNAME_REPLACE, '@user', text)
    text = text.strip('"')
    return text


def preprocess(filename):

    def make_vocab():
        text = open(filename, 'r').read()
        chars = list(set(text))
        ch2ix = {ch: ix for ix, ch in enumerate(chars)}
        url_codepoint, url_char = find_replacement_char(ch2ix)
        max_ix = len(ch2ix)
        assert max_ix not in ch2ix.values()
        ch2ix[url_char] = max_ix
        return ch2ix, url_char

    ch2ix, url_char = make_vocab()
    print('vocab size: %d' % (len(ch2ix)))
    # write vocabulary
    outfile = '%s.vocab' % filename
    with open(outfile, 'w', encoding='utf8', newline='\n') as fw:
        for ch, ix in ch2ix.items():
            codepoint = ord(ch)
            line = '%d\t%d\n' % (codepoint, ix)
            fw.write(line)

    # write clean lines
    outfile = '%s.clean' % filename
    with open(outfile, 'w', encoding='utf8', newline='\n') as fw:
        with open(filename, 'r') as fr:
            for line in fr.readlines():
                # line = line.rstrip('\n')
                clean_line = clean_tweet(line, url_char)
                if len(clean_line) > 0:
                    fw.write(clean_line + '\n')


def test_output(filename):
    clean_file = '%s.clean' % filename
    with open(clean_file, 'r') as fr:
        ct = 0
        for line in fr.readlines():
            print(line)
            ct += 1
            if ct >= 10:
                break


if __name__ == '__main__':
    preprocess(sys.argv[1])
    test_output(sys.argv[1])




# def preprocess(filename):
#
#     def step1(filename):
#         if not filename.endswith(ENCODING):
#             filename = convert(filename)
#         text = codecs.open(filename, 'r', ENCODING).read()
#         chars = list(set(text))
#         ch2ix = {ch: ix for ix, ch in enumerate(chars)}
#         url_char, url_cp, ch2ix = find_replacement_char(ch2ix)
#         return filename, url_char, ch2ix
#
#     filename, url_char, ch2ix = step1(filename)
#     clean_fname = '%s.clean' % filename
#     fw = codecs.open(clean_fname, 'w', ENCODING)
#     with codecs.open(filename, 'r', ENCODING) as fr:
#         for line in fr.readlines():
#             line = line.rstrip('\n')
#             cline = clean_tweet(line, url_char)
#             fw.write(cline + '\n')
#     fw.close()
#     vocab_fname = '%s.vocab' % filename
#     write_char_vocab(ch2ix, vocab_fname)
#     return clean_fname
#
#
# def clean_tweet(text, url_char):
#     text = text.strip('"')
#     url = re.compile(r'http[s]?://t.co/\w+', flags=re.UNICODE)
#     text = re.sub(url, url_char, text)
#     return text
#
#
# def convert(filename):
#     orig = codecs.open(filename, 'r', 'utf8').read()
#     uni = unicode(orig)
#     converted_filename = '%s.%s' % (filename, ENCODING)
#     with codecs.open(converted_filename, 'w', ENCODING) as fw:
#         fw.write(uni)
#     return converted_filename
#
#
# def write_char_vocab(ch2ix, outfile):
#     with open(outfile, 'w') as fw:
#         for ch, ix in ch2ix.items():
#             codepoint = ord(ch)
#             line = '%d\t%d\n' % (codepoint, ix)
#             fw.write(line)
#
# if __name__ == '__main__':
#     preprocess(sys.argv[1])
