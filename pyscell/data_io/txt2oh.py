from util import *
import numpy as np


def from_file_simple(txtfile, ch2ix, buffsize):
    V = len(ch2ix)
    txt = codecs.open(txtfile, 'r', 'utf8').read()
    ixes = [ch2ix[ch] for ch in txt]
    N = len(ixes)
    curr = 0
    end = curr+buffsize
    while end < N:
        chunk = ixes[curr:end]
        oh = np.zeros((buffsize, V), dtype='int32')
        for j, ix in enumerate(chunk):
            oh[j, ix] = 1
        yield oh
        curr = end
        end = curr+buffsize


def from_file(txtfile, ch2ix, buffsize=4096):
    """
    Big matrix of one-hots representing characters, no sentence/tweets boundaries
    """
    v = len(ch2ix)

    def lookup(dic, chs):
        return map(lambda ch: dic[ch], chs)

    with codecs.open(txtfile, 'r', 'utf8') as fb:
        chunk = fb.read(buffsize)
        while chunk and len(chunk) >= buffsize:
            n = len(chunk)
            ixes = lookup(chunk, ch2ix)
            oh = np.zeros((n, v), dtype='int32')
            for j, ix in enumerate(ixes):
                oh[j, ix] = 1
            yield oh
            chunk = fb.read(buffsize)


def from_file_ixes(txtfile, ch2ix, buffsize=4096):
    """
    Big matrix of indices representing characters, no sentence/tweets boundaries
    """
    with codecs.open(txtfile, 'r', 'utf8') as fb:
        chunk = fb.read(buffsize)
        while chunk and len(chunk) >= buffsize:
            ixes = np.zeros((len(chunk)), dtype='int32')
            for j, ch in enumerate(chunk):
                if ch not in ch2ix:
                    print 'not found: %d %s' % (ord(ch), ch)
                    raise ValueError()
                ix = ch2ix[ch]
                ixes[j] = ix
            yield ixes
            chunk = fb.read(buffsize)
