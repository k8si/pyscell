# from util import url_char, readlines, ENCODING
# import re
# import codecs
# import sys
#
#
# def clean_tweet(text):
#     text = text.strip('"')
#     url = re.compile(ur'http[s]?://t.co/\w+', flags=re.UNICODE)
#     URL_CHAR = url_char()
#     text = re.sub(url, URL_CHAR, text)
#     return text
#
#
# def clean(filename):
#     if not filename.endswith(ENCODING):
#         filename = convert(filename)
#     clean_filename = '%s.clean' % filename
#     writer = codecs.open(clean_filename, 'w', ENCODING)
#     with codecs.open(filename, 'r', ENCODING) as fr:
#         for line in fr.readlines():
#             cline = clean_tweet(line)
#             writer.write(cline)
#     writer.close()
#     return clean_filename
#
#
# def convert(filename):
#     orig = codecs.open(filename, 'r', 'utf8').read()
#     uni = unicode(orig)
#     converted_filename = '%s.%s' % (filename, ENCODING)
#     with codecs.open(converted_filename, 'w', ENCODING) as fw:
#         fw.write(uni)
#     return converted_filename
#
#
# if __name__ == '__main__':
#     nargs = len(sys.argv)
#     clean(sys.argv[1])
#
#
