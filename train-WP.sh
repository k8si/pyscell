#!/usr/bin/env bash

# train LSTM on 'War and Peace' using Karpathy defaults

dir="warandpeace"
train="$dir/WP.txt.train"
val="$dir/WP.txt.val"
test="$dir/WP.txt.test"
vocab="$dir/vocab.txt"

python pyscell/train_lstm.py $train $val $test $vocab \
--batch-size 100 \
--decay 0.95 \
--dropout 0.1 \
--early-stop 5 \
--learning-rate 0.002 \
--nlayers 2 \
--no-decay-epochs 10 \
--nrec 64 \
--num-epochs 50 \
--seqlen 100 \
--val-freq 1000




