#!/usr/bin/env bash

model="debug/emb_model.pkl"
vocab="debug/vocab.txt"
outfile="debug/chars.emb"

python pyscell/write_embeddings.py $model $vocab $outfile \
--bidir 1 \
--nlayers 2 \
--nrec 64 \
--seqlen 100


